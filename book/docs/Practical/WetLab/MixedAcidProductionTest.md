# Mixed Acid Production Test

## Introduction
Mixed acid fermentation is an anaerobic process that can occur in anaerobically growing bacteria. In this fermentation process, a carbohydrate (e.g. glucose) is converted into a mixture of organic acids. The composition of the organic acid mixture - e.g., which organic acids, what concentrations - depends on the energy metabolism of the microbe. A typical mixture of products consists of lactate, acetate, succinate, formate, ethanol, and the gases H<sub>2</sub> and CO<sub>2</sub>. The energy metabolism - and consequently the products formed - depends on the enzymes that can be expressed by the organism. For instance lactate dehydrogenase, acetate kinase, and formate hydrogen lyase are all involved in the formation or transformation of organic acids. The produced organic acids (for instance, lactic acid) can dissociate into their conjugate salt (e.g., lactate) and a proton (H<sup>+</sup>). The latter results in acidification of the medium, and a decrease in pH.

Acid production by bacteria can be tested by cultivation in MRVP broth. This medium contains glucose as carbon source, peptone to supply nitrogen and a phosphate buffer. This buffer ensures that pH only decreases when a large amount of acids is produced. This helps differentiate anaerobically fermentating & growing organisms from the organisms that do ferment glucose but may still produce small amounts of acids. The pH of the medium will be measured after 24 and 48 hours of culturing.

## Protocol
### Day 1: Set up experiment
* Inoculate a tube with MRVP medium with 200 µL overnight culture and add one centimeter of paraffin oil on top. Culture overnight at optimal temperature for your strain whithout shaking.

### Day 2: Observe results
* Transfer 0.5 ml from the culture to a sterile Eppendorf. Put the tube with the culture back in the incubator.

* Measure the pH from the culture using a pH strip.

* Incubate the rest of the culture for an additional 24 hours.

### Day 3: Observe results
* Measure the pH of the culture that has now grown for 48 hours with a pH strip.

## References

Madigan, M. T., Martinko, J. M., Bender, K. S., Buckley, D. H., & Stahl, D. A. (2015). *Brock biology of microorganisms* (Fourteenth edition.). Boston: Pearson.

Ingrid M. Keseler, Amanda Mackie, Martin Peralta-Gil, Alberto Santos-Zavaleta, Socorro Gama-Castro, César Bonavides-Martínez, Carol Fulcher, Araceli M. Huerta, Anamika Kothari, Markus Krummenacker, Mario Latendresse, Luis Muñiz-Rascado, Quang Ong, Suzanne Paley, Imke

Schröder, Alexander G. Shearer, Pallavi Subhraveti, Mike Travers, Deepika Weerasinghe, Verena Weiss, Julio Collado-Vides, Robert P. Gunsalus, Ian Paulsen, Peter D. Karp (2013) EcoCyc: fusing model organism databases with systems biology. Nucleic Acids Res 41 (D1): D605-D612
