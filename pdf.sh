DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $DIR

jupyter-book build --builder pdflatex book

cp $DIR/book/_build/latex/book.pdf $DIR/reader.pdf
