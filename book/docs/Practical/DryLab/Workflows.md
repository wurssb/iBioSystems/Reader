# Galaxy workflows

One of the most powerful features of Galaxy is the ability to create workflows. A workflow is a series of steps that are executed in a specific order. This can be useful when you have a series of tools that need to be executed in a specific order. For example, you might want to trim your reads, align them to a reference genome, and then call variants. You can create a workflow that does all of these steps in the correct order.

In this section, we will show you how to create a simple workflow in Galaxy. We will create a workflow that takes a FASTQ file as input, performs a quality assessment, does the assembly, and then reports the results.

## Creating a new workflow

To create a new workflow, click on the `Workflow` tab in the top menu bar. This will take you to the workflow editor. Click on the `Create` button to create a new workflow. You will be prompted to give your workflow a name (e.g., Assembly). Enter a name for your workflow and click `Create`.

## Adding steps to the workflow

Once you have created your workflow, you can start adding steps to it. To add a step, click on the tool that you want to add to the workflow. Once clicked, the tool will be added to the workflow editor. You can then connect the tools together by clicking on the output of one tool and dragging it to the input of another tool. This will create a connection between the two tools.

For our workflow, we will add the following steps:

1. Input dataset: For the forward reads
2. Input dataset: For the reverse reads
3. Input dataset: For the nanopore reads (optional)

4. Quality assessment: FastQC
5. Quality assessment: FastQC (for the reverse reads)

6. Assembly: SPAdes (for the Illumina reads and the nanopore reads if available)

7. Spades size and coverage filter
8. Assembly report (before and after filtering): QUAST

![Workflow example](../../Figures/workflows.png)
Example of a potential workflow for assembly and quality assessment of Illumina and Nanopore reads. You can test this with the example data 50x coverage of example dataset provided in the assembly tutorial.