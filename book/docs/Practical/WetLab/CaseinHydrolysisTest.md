# Casein Hydrolysis Test

## Introduction
Casein is a protein that gives milk its characteristic white color, making up 85% percent of the protein content of milk. Due to its large size, casein cannot enter bacterial cells. In order to utilize casein, bacterial cells secrete extracellular proteolytic enzymes (specifically, caseinases and peptidases) that hydrolyze casein to amino acids. These amino acids are in turn easily transported into the cell where they are metabolized. 

The presence of caseinase activity can be examined by culturing an organism on milk agar. Milk agar has a milky white appearance. Caseinase-positive microorganisms will locally hydrolyse the casein in the plate forming clear zones around bacterial colonies. This reaction is made more visible by a coloured reagent (bromocresol green) that turns colourless during casein hydrolysis.

## Protocol
### Day 1: Set up experiment

* Streak a few bacterial colonies from a plate to a coloured milk agar plate and incubate overnight.

* **Tip**: Avoid streaking the entire surface of your plate, as it may make it harder to observe results. Streak single lines of your culture.

### Day 2: Observe results

**Clear:** Casein is degraded (presence of enzyme with caseinase activity). 

**Milky coloured appearance:** Casein is not degraded (absence of enzyme with caseinase activity). 

## References

Brown, A. E. (2009). Benson's Microbiological Applications: Laboratory Manual in General Microbiology (Vol. 11). New York, MA: McGraw-Hill Companies.

Cappuccino, J. G., & Sherman, N. (2008). Microbiology: a laboratory manual (Vol. 8). San Francisco, MA: Pearson/Benjamin Cummings.

Harley, J. P., and Prescott, L. M. (2002). Laboratory Exercises in Microbiology (Vol. 5). New York, MA: McGraw-Hill Higher Education.
