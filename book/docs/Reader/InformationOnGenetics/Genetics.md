# Genetics

The genome of a microorganism contains all of its genetic information. Using sequencing technology, we are able to map the full genome. 

But knowing the full genome of a microorganism does not mean we willl fully understand what its capable of and what it will do under different circumstances. Additional research is needed to better understand the microorganism. This can be done with various -omics technologies.

-Omics is a collective term for the characterization and quantification of large amounts of biological molecules. These biological molecules are sub-divided into DNA, RNA, proteins and metabolites. In this course, we will only focus on the genomics part. Transcriptomics, proteomics and metabolomics are (currently) too expensive and too complicated and are therefore beyond the scope of this course. You will do different experiments to test the physiology of your microorganism. 

During this course, you will receive the sequenced data which must be assembled and annotated. And you will notice that many things can go wrong during the annotation process. In Brock, you will read the biological factors causing these errors.

During the annotation, you will be looking for ORFs, of which there are roughly a thousand of per Mb of sequence for prokaryotes. Eukaryotes have less per Mb as they have more non-coding regions within their genome. 

The size of genomes varies per micro-organism. In Brock examples are given how the lifestyle of an organism affects its genome size and how this can give information about the background of your microorganism. 

**Course material:**

* Brock edition 14. Chapter 6.1, 6.2, 6.3, 6.4
* Brock edition 15. Chapter 9.1, 9.2, 9.3

**Key questions:**

After reading the course material you should be able to answer the following key questions. 

1. Explain which types of -omics technologies are available to test an (micro)organism.
2. Explain in 5 sentences how Sanger sequencing works.
3. What is an ORF?
4. How are the non-coding regions of Eukaryotes named?
5.	A microorganism living in symbioses with a host has usually a large / average / small size genome.
