# Functional Omics

As mentioned before, knowing the genome doesn’t say much about how a microorganism will behave under different circumstances. When, which gene is brought to expression depends on a lot of factors. And if a microorganism wants to, for example, covert a substance within the cell it has to take several steps. First RNA is transcribed from the DNA. The RNA is translated into an enzyme. Finally, the enzyme converts the substance. It is clear that the whole process is bigger the only the genome.
Multiple –omics technologies are available to understand what a microorganism does under certain circumstances. Genomics looks at which genes are present within the organism. Transcriptomics looks at which genes are transcribed. Proteomics looks at the proteins present within the cells. And metabolomics looks at the metabolites which are present in the organism. Analysing samples from different growth conditions gives insight into the behaviour of the organisms. 
There is one other technology but it does not fit the cascade as described above where all techniques are applied at one species of microorganism. Metagenomics looks at all the sequences of different species present within a sample. In this way, you can look which species are present in sea water and how abundant they are. This enables microbiologists to detect microorganisms that are unable to grow under laboratory conditions.  

**Course material:**

* Brock edition 14. Chapter 6.7, 6.8, 6.9, 6.10
* Brock edition 15. Chapter 9.8, 9.9, 9.10, 9.11


**Key questions:**

After reading the course material you should be able to answer the following key questions. 

1.	Which techniques are available to investigate the transcriptome.
2.	Explain in a couple of sentences the principle of MALDI-TOF
3.	What is an interactome?
4.	Name 3 metabolites present in a microorganism.
5.	Can DNA and proteins be extracted from the same sample?
