# Gram staining

## Introduction 

The Gram stain was introduced by the Danish microbiologist Hans Christian Gram in 1884. It classifies bacteria into two large groups depending on their cell wall structure (See Figure).

Gram-positive bacteria have single cell membrane with a thick outer layer of peptidoglycan molecules. On the other hand, Gram-negative bacteria have a double cell membrane, with a thin peptidoglycan layer in between. The outer membrane also contains lipopolysaccharides (LPS), lipoproteins and other macromolecules.

![](GramStaining.png)

*Figure: Schematic representation of Gram-positive (right) and Gram-negative (left) bacterial cell walls*

The Gram stain is an easy, differential staining procedure that stains cells as either purple (Gram-positive) or red (Gram-negative bacteria). In some cases, Gram-positive bacteria can stain as a mixture of Gram-positive and Gram-negative bacteria, making them Gram-indeterminate. This is due to loss of purple stain because of lower amounts of peptidoglycan in their cell walls. Because peptidoglycan layer thickness decreases in old Gram-positive cultures of e.g., Bacillus, Clostridium, and Butyrivibrio, such old cells may yield a false Gram-negative result (Beveridge, 1990).



![](GramStaining2.png)

*Figure: Result of a Gram staining of a mixed culture. Tiny red cells are Gram-negative, large purple cells are Gram-positive.*

The work flow is as follows: After heat fixation, crystal violet stains all cells purple. Iodine is then used to fixate the crystal violet inside Gram-positive cells. Subsequently, decolorizer is used to remove the stain from Gram-negative cells by penetrating the outer membrane. In Gram-positive cells the decolorizer has an opposite effect: dehydration of the cell wall, closing pores in it, and thus preventing the crystal violet from escaping the cells. In the last step, safranin is added to stain Gram-negative cells red. A typical result for a mixed culture of Gram-positive and Gram-negative cells is shown in the Figure.

## Protocol

Negative and positive controls are available in the lab.

### Slide preparation & Heat fixing
* With a sterile loop place a drop of sterile water on a microscope slide. 
* Take a new sterile inoculation loop and pick a very small sample of a bacterial colony from the plate and gently stir into the drop of water on the slide to create an emulsion. 
    * **Important**: Use young cultures (24-36 hours) to prevent false Gram-negative results.
    * **TIP**: Don't use too much cells. If you can see them with the naked eye, it is likely too much.
* Place the microscope slide with the smear side up on a heating plate until the smear is dry (a few minutes).

**Label your slide carefully (sticker and pencil (or) ballpoint)!**

### Gram stain procedure 

* Put the microscope slide across a staining tray that stands on a piece of filter paper.

* Add drops of crystal violet to cover the smear and let stand for 1 minute. 

* Hold the slide upside down and gently rinse with running tap demi water. 

* Put the slide back across the staining tray and add drops of iodine to cover the smear and let stand for 1 minute. 

* Hold the slide upside down and gently rinse with running tap demi water. The smear will appear as a purple circle on the slide. 

* Put the slide back across the staining tray and tilt it slightly. Apply the decolorizer solution drop by drop until the decolorizer solution runs almost clear. 
    * **TIP**: Be careful not to over-decolorize, or you may decolorize Gram-positive cells. 

* Immediately hold the slide upside down and gently rinse with running tap demi water.
* Add drops of safranin to counter-stain and let stand for 30 seconds. 

* Hold the slide upside down and gently rinse with running tap demi water. Air dry the slide. 

* View the smear using a light microscope under oil-immersion.



## References
Beveridge TJ. (1990). Mechanism of gram variability in select bacteria. J Bacteriol 172(3), 1609-1620.

Fisher, J., & Mobashery, S. (2010). Host−Guest Chemistry of the Peptidoglycan. Journal Of Medicinal Chemistry, 53(13), 4813-4829.

König, H., Claus, H., & Varma, A. (2010). Prokaryotic cell wall compounds (pp. 383-406). Berlin: Springer.
