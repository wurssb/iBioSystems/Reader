# DNA Extraction

## Introduction 

DNA harbors the genetic information for cell development and behavior. Extraction and purification of genomic DNA are essential procedures in molecular biology to link the genotype with the phenotype. The principles of DNA extraction include several steps. 

1. Break or lyse the cells containing the DNA of interest. 

2. Degrading DNA associated proteins as well as other cellular proteins. 

3. Precipitating and purifying genomic DNA. 

4. Eluting genomic DNA with buffer solution. 

In this experiment, you will isolate genomic DNA from your bacterial sample that can be used for whole genome sequencing. Commercial DNA isolation kits are available that ensure high quantity and quality of genomic DNA. The protocol below makes use of such a kit.

## Protocol

### Preparations

**Preheat a water bath or heating block to 55 °C**

- For use with both gram-positive and gram-negative bacteria.  

**Preheat a water bath or heating block to 37 °C**

- For use with gram-positive bacteria only.

**Overview of the solutions used in the protocol**
|Code   |Solution   |
|-------|-----------|
|T      |Lysis solution T|
|R      |RNAse A    |
|K      |Lysis solution C|
|LS     |Lysozyme   |
|LL     |Lysozyme + lysostaphin|
|ML     |Lysozyme + mutanolysin|
|CP     |Column preparation solution|
|ET     |100% ethanol|
|W1     |Wash solution 1|
|W      |Wash solution|
|EL     |Elution solution|

**Procedure**

- For gram-negative bacteria start with Procedure A
- For gram-positive bacteria start with Procedure B

### A. Gram-Negative Bacterial Preparation

**1a. Harvest Cells**

Pellet 1.5 mL of an overnight bacterial broth culture by centrifuging for 2 minutes at 12,000–16,000 × g. Remove the culture medium completely and discard.

**Note:** If bacteria are propagated in rich media it will be necessary to reduce the volume of starting material to 0.5 mL of an overnight bacterial broth culture to avoid overloading the GenElute columns.

**2a. Resuspend Cells**

- Resuspend the pellet thoroughly in 180 μL of Lysis Solution T. 
- Add 20 μL of RNase A Solution, to make RNA-free genomic DNA. mix, and incubate for 2 minutes at room temperature, then continue with step 3a.

**3a. Prepare for Cell Lysis**

- Add 20 μL of the Proteinase K solution to the sample. Mix and incubate for 30 minutes at 55 °C.

**4a. Lyse Cells**

- Add 200 μL of Lysis Solution C, vortex thoroughly (about 15 seconds), and incubate at 55 °C for 10 minutes.
- A homogeneous mixture is essential for efficient lysis. Continue with Procedure C - step 5.

### B. Gram-Positive Bacterial Preparation

**1b. Harvest Cells**

Pellet 1.5 mL of an overnight bacterial broth culture by centrifuging for 2 minutes at 12,000–16,000 × g. Remove the culture medium completely and discard. 

**Note:** If bacteria are propagated in rich media, it will be necessary to reduce the volume of starting material to 0.5 mL of overnight bacterial broth culture to avoid overloading the GenElute columns.

**2b. Resuspend Cells**

Resuspend the pellet thoroughly in 200 μL of Lysozyme Solution (see  step 1b) and incubate for 30 minutes at 37 °C.
Add 20 μL of RNase A Solution, to make RNA-free genomic DNA and incubate for 2 minutes at room temperature, then continue with step 4b.

**3b. Lyse Cells**

Add 20 μL of the Proteinase K solution to the sample, followed by 200 μL of Lysis Solution C. Vortex thoroughly (about 15 seconds) and incubate at 55 °C for 10 minutes. A homogeneous mixture is essential for efficient lysis. Continue with Procedure C - step 5.


### C. DNA Isolation from Gram-Positive and Gram-Negative Bacteria

This is a continuation of the procedure from the lysates prepared in steps 1–4a and/or steps 1–3b.

**5. Column Preparation**

***WARNING: Pay attention that in this step you are working with the column only***

Add 500 μL of the Column Preparation Solution to each pre-assembled GenElute Miniprep Binding Column seated in a 2 mL collection tube. 
Centrifuge at 12,000 × g for 1 minute. Discard the eluate.
Note: The Column Preparation Solution maximizes binding of DNA to the membrane resulting in more consistent yields.

**6. Prepare for Binding**

Add 200 μL of ethanol (95–100%) to the lysate and mix thoroughly by vortexing for 5–10 seconds. A homogeneous mixture is essential.

**7. Load Lysate**

Transfer the entire contents of the tube into the binding column. Centrifuge at ≥ 6500 × g for 1 minute. Discard the collection tube containing the eluate and place the column in a new 2 mL collection tube.

**8. First Wash**

Add 500 μL of Wash Solution 1 to the column and centrifuge for 1 minute at ≥ 6500 × g. Discard the collection tube containing the eluate and place the column in a new 2 mL collection tube.

**9. Second Wash**

Add 500 μL of Wash Solution to the column and centrifuge for 3 minutes at maximum speed (12,000–16,000 × g) to dry the column. The column must be free of ethanol before eluting the DNA.
Centrifuge the column for an additional 1 minute at maximum speed if residual ethanol is seen. You may empty and re-use the collection tube if you need this additional centrifugation step. Finally, discard the collection tube containing the eluate and place the column in a new 2 mL collection tube.

**10. Elute DNA**

Pipette 200 μL of the Elution Solution directly onto the center of the column; incubate for 5 minutes at room temperature after adding the Elution Solution, then centrifuge for 1 minute at ≥ 6500 × g to elute the DNA. 

### Results

The concentration and quality of the genomic DNA can be determined with the Nanodrop spectrophotometer:
* Clean the Nanodrop sensor with a dust-free wipe
* Deposit 1.5 µL of eluted DNA on the sensor
* Measure absorbances at 260 nm, 280 nm & 320 nm. 

A<sub>260</sub> & A<sub>280</sub> should be between 0.1 and 1.0, or within the linear range of the device. A<sub>320</sub> absorbance is used to correct for background absorbance. The (A<sub>260</sub>-A<sub>320</sub>)/(A<sub>280</sub>-A<sub>320</sub>) should be 1.6–1.9 for sufficiently good-quality DNA for sequencing.

*Table: Typical DNA Yield with the GenElute Bacterial Genomic DNA Kit*
![](DNAExtraction.png)

## References

Harley, J. P., &amp; Prescott, L. M. (2008). Laboratory Exercises in Microbiology. Boston: McGraw Hill-Higher Education.

Michael, T. M., John, M. M., & Jack, P. (2002). Brock microbiology of microorganism. New Jersey. ISBN, 10, 0130662712.
