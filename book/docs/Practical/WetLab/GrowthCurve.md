# Growth curve

## Introduction
Microbes in an environment with growth substrates will repeatedly double, resulting in an increase in the number of cells in a population. Microbial growth can be studied by generating and analyzing a growth curve of a bacterial culture (Figure 1). This approach requires methods to quantify the number of bacteria in a liquid growth medium. While many approaches are available - each with their own advantages and disadvantages - one of the most common approaches is to measure absorbance (specifically, optical density at 600 nm, or OD<sub>600</sub>) of the medium throughout growth. While this metric is not a perfect representation of cell count in the medium, it is easy to measure and can be measured in parallel in 96 - or even 384 - conditions at once using a microplate spectrophotometer. Plotting the obtained OD<sub>600</sub> over time results in a sigmoid curve with four different phases: 

**Lag phase:** The period needed for the cells to adapt to the new environment. The cellular metabolism is accelerated and cells increase in size but they are not able to replicate yet. 

**Log (exponential) phase:** Cells grow and divide at their maximum rate in the given conditions (medium composition, temperature, etc.). The total number of cells increases with time. From the exponential phase, the growth rate (*µ*) and doubling time (*dt*) of the cells - i.e., how long on average it takes to double - of a microbe can be calculated. Note that *μ* is related to *dt*: *dt* = ln2/μ. Example: a bacterium with doubling time 30 minutes (0.5 hours) will have a growth rate of ln2/0.5=1.39h<sup>-1</sup>.

**Stationary phase:** Bacterial growth becomes limited due to depletion of essential nutrients or accumulation of (toxic) waste compounds. (Exponential) growth stops and there is no longer a net increase or decrease of cell number. The growth rate of the population is zero. While growth stops, cells can still be metabolically active in this phase.

**Death (decline) phase:** The depletion of nutrients and/or the ongoing accumulation of waste and toxic compounds force the bacterial population into the death phase, where the number of dead cells exceeds the number of viable cells.  

<span style="color:white">\newpage</span>

![](GrowthCurve.png)

*Figure 1: Growth curve of a liquid bacterial culture. Four different phases can be distinguished. Lag phase, exponential phase, stationary phase and the decline or death phase. The curve is obtained by plotting the bacterial optical density versus the time, which is represented in hours.*

## Protocol
### Day 1: Set up experiment
* Measure the OD of an overnight LB-medium culture, not older than 24-36h
* Inoculuate your liquid culture in fresh LB-medium so you have at least 2 mL with a final OD<sub>600</sub> = 0.05.
* Pipet 200 μL of the bacterial culture with OD600 of 0.05 in each of the 3 wells that are marked with your group number (See Figure 2).
    * Row A of the 96 well plate will be filled with fresh, uninoculated medium. This will serve as the negative control & blank measurement.  
* The 96-well plate will be placed in a temperature-controlled microplate spectrophotometer by a supervisor and growth will be followed for approximately 18 hours.
    * Inoculate both 96-well plates in the lab. These will be incubated at 30°C & 37°C.

### Day 2: Analyze results
* Download your growth curve data from Brightspace.
    * Make a graph plotting the average OD<sub>600</sub> from your 3 wells over time.
    * **Important**: Correct the OD<sub>600</sub> of each well with the average OD of the negative control in row A.
* Using the Excel spreadsheet on Brightspace, calculate the growth rate of the bacterial cultures.
    * Calculate µ for data from each well individually.
    * Calculate the average µ and standard deviation over the 3 wells.

![](96Wells.png)
*Figure 2: Schematic drawing of a 96 wells plate. B: blank; 1-24: group numbers.*

## References

Madigan, M., Bender, K., Buckley, D., Martinko, J., Brock, T. & Stahl, D. (2014). Brock biology of microorganisms (14th ed., pp.175-176).
