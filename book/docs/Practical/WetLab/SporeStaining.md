# Spore Staining

## Introduction 

Certain bacterial genera (for instance Bacillus & Clostridium) produce structures called endospores when exposed to harsh environmental conditions, a process known as sporulation. Endospores ("endo" from the Greek prefix "ενδο" meaning "within") are highly resistant to heat, drought, radiation, harsh chemicals and nutrient depletion, allowing survival under unfavorable growth environments. Spores may be located in the middle (central), at the end (terminal), or between the middle and the end of the cell (subterminal/lateral), and can be spherical or elliptical in shape, and vary in size. During harsh conditions, these spores can be released from the mother cell, or the cell can lyse (i.e., disintegrate), causing release of the spore.

The outer layer of a spore is composed of keratin, a protein that hampers mild staining procedures. To overcome this limitation, the Schaeffer-Fulton method is commonly used. During this procedure, malachite green stain is forced into the spore by steaming a heat-fixed bacterial smear. Malachite green has low affinity for cellular material and is water-soluble. Consequently, washing with water can be used to decolorize vegetative cells, followed by counterstaining with red-colored stain safranin (See Figure).

![](Spores.png)

*Figure: Spore stain result. Spores are stained green, vegetative cells and spore mother cells are stained.*


<span style="color:white">\newpage</span>

## Protocol

* Using a sterile inoculation loop, place a small drop on a microscope slide of an old liquid culture or make a suspension from a colony from an old plate. Spread the drop by means of circular motion of the inoculation loop to about 1cm in diameter.
* Air dry the slide and place it with the smear side up on a heating plate for fixation.
* Flood the slide with Schaeffer & Fulton's Spore Stain A solution (malachite green). 
* Steam for 4-6 minutes, and rinse upside down under running tap demi water. 
* Counterstain with Schaeffer & Fulton's Spore Stain B solution (safranin) for 30 seconds. 
* Wash upside down with running demi tap water, air dry and observe under light microscope (oil immersion).

## References

Lillie, R. D. (1977). *H. J. Conn's Biological Stains* (Vol 9).

Leboffe M.J. & Pierce B.E. (2010) Microbiology Laboratory Theory and Application 3rd edition. Morton Publishing Company. Englewood, Colorado. Pages 36-47.
