# Genome based Taxonomy

<!-- As there are many different protein functions available it is crucial to identify the right function for your proteins. In the previous approach you have used BLAST to find similar DNA sequences but you can also use BLAST to find protein sequences in a database. This allows you to identify possible function(s) for a given set of proteins. BLASTP uses a more sophisticated lookup mechanism in contrast to BLASTN where you can only have a match/mismatch or a gap. With BLASTP you can have mismatches that are biochemically nearly the same or very different. Using scoring schemes variations between amino acid hits can give positive or negative scores and based on that homology is inferred.

Beside comparing protein sequences you can also search for functional patterns inside a protein sequence. This is a very sensitive aproach and might give you a more specific function of a protein of interest. 

**Course material:**

* Practical Bioinformatics 2012. Chapter 4
* Practical Bioinformatics 2012. Chapter 8.1 - 8.3
* Presenation on BLASTP
* Read A Critical Guide to BLAST [https://doi.org/10.7490/f1000research.1116052.1](https://doi.org/10.7490/f1000research.1116052.1)

**Key questions:**

* What type of BLAST is used for comparing protein sequences against a protein database?
* Which BLOSUM matrix is preferred when performing a BLAST?
* Why are patterns more sensitive than BLAST?
* Name two different similarity matrices
* What is degeneracy?
* Why does tryptophane have such a high similarity score with itself but not with others? 
-->

## Practical

### Genome Analysis

#### classification based on whole genomes

- Now try to stablish the phylogeny of the different species by using the whole genomes of the Trichococcus example from exercise 1?
- Reflect: what would be the classification based on the genome?
- Note: Fasta files can be downloaded from the links in the table

Many online resources allows you to calculate DNA/DNA-hybridization using online tools...
Basic principles: Upload your genome and select or upload reference genome(s) to compare against

- For the dDDH (GGDC 3.0): http://ggdc.dsmz.de/
- For the ANI: http://jspecies.ribohost.com/jspeciesws/
- For AAI: http://enve-omics.ce.gatech.edu/aai/

Use any of the above websites to perform the comparison between the different genomes.


| Strain                      | [T. Ilyis R210T](https://www.ebi.ac.uk/ena/data/view/FJNB01000001-FJNB01000044) | [T. pasteurii  DSM 2381T](https://www.ebi.ac.uk/ena/data/view/FWEY01000001-FWEY01000021) | [T. flocculiformis  DSM 2094T](https://www.ebi.ac.uk/ena/data/view/FOQC01000001-FOQC01000110) | [T. collinsii  DSM 14526T](https://www.ebi.ac.uk/ena/data/view/FNQH01000001-FNQH01000024) | [T. palustris  DSM 9172T](https://www.ebi.ac.uk/ena/data/view/FOTD01000001-FOTD01000052) | [T. patagoniensis  DSM 18806T](https://www.ebi.ac.uk/ena/browser/view/GCA_003051085.1) |
|-----------------------------|:------------------------------------------------------------------------------:|:-------------------------------------------------------------------------------------:|:------------------------------------------------------------------------------------------:|:--------------------------------------------------------------------------------------:|:-------------------------------------------------------------------------------------:|:-----------------------------------------------------------------------------------:|
| T. Ilyis R210T              |                                                                                |                                                                                       |                                                                                            |                                                                                        |                                                                                       |                                                                                     |
| T. pasteurii DSM 2381T      |                                                                                |                                                                                       |                                                                                            |                                                                                        |                                                                                       |                                                                                     |
| T. flocculiformis DSM 2094T |                                                                                |                                                                                       |                                                                                            |                                                                                        |                                                                                       |                                                                                     |
| T. collinsii DSM 14526T     |                                                                                |                                                                                       |                                                                                            |                                                                                        |                                                                                       |                                                                                     |
| T. palustris DSM 9172T      |                                                                                |                                                                                       |                                                                                            |                                                                                        |                                                                                       |                                                                                     |
| T. patagoniensis DSM 18806T |                                                                                |                                                                                       |                                                                                            |                                                                                        |                                                                                       |                                                                                     |

### Whole-genome phylogenetic reconstruction

Many online resources allows you to calculate DNA/DNA-hybridization using online tools…
Basic principles: Upload your genome and select or upload reference genome(s) to compare against

- For the dDDH: http://ggdc.dsmz.de/
- For the ANI: http://jspecies.ribohost.com/jspeciesws/
- For AAI: http://enve-omics.ce.gatech.edu/aai/

***Analyse your genome using dDDH and ANI***


<!-- #### GTDB-Tk
- Go to https://narrative.kbase.us
- Sign up (either Google or ORCID - if you prefer ORCID: https://orcid.org/signin)
- Sign in
- Click on **New narrative**
    - Add data
    - Import, Select from computer
    - Select **FASTA assembly file**
    - Select **Assembly** at **Import As..."**
    - Click **Upload** button (upward arrow) 
    - Run
- Apps
    - "Genome annotation" 
    - Classify Microbes with GTDB-Tk
    - Select your assembly at "Assembly input"
    - "Run"
- While you wait, go to next exercise
- Click on "Result" and interpret the taxonomy of your microbe -->

#### GToTree

- Go to Brightspace and download tree ("gtotree_1500g_allRanks.tre.rooted")
- Download the software: https://github.com/rambaut/figtree/releases
- Open tree
    - Click OK.
- Maximise window and adjust "Expansion"
- Branch labels 
    - Click label
    - Tree
    - Order nodes
- Find your strain (either manually or through the Search option) and interpret its taxonomy.

#### ITOL

- Go to https://itol.embl.de/. 
- Sign up/Sign in
- Tree upload
    - Upload downloaded tree
- Rectangular mode. 
- Advanced 
    - Bootstrap/Metadata: Display. 


## Classify

With the information learnt and the obtained results (e.g. values, figures) 
classify your strain by both 16s rRNA gene analysis and whole genome analysis.

## Tomorrow

Please run the following job today so we can start tomorrow immediatly!

### EGGNOG

Due to accidentally overloading the KEGG KAAS system we have decided to use EggNOG as an alternative annotation application. We will still use KEGG for the mapping of the results obtained in EggNOG.

- Go to http://eggnog5.embl.de
- Click on the Eggnog mapper (batch functional annotation)
- Upload your protein sequences (as you can see you can also upload other file types)
- Enter your email
- Submit
- Check the email, click on the link and make sure you start the job!

```{note}
When Eggnog states it is finished you can check the files under the `Access your job files here` link in the job overview.
At somepoint (could be easily 10/20min later) a `.xlsx` should appear with your results (After the job is done!)
```