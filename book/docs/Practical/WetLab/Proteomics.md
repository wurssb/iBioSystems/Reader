# Protein Extraction and Quantification

## Introduction 
Proteomics has emerged as a powerful new tool to characterize the physiology and metabolic activity of microbes. This technique can analyze which proteins are present in a cell, which means it more closely reflects the actual metabolic activity & enzymatic conversions taking place in a cell than genomics (DNA sequences; what could be done by a cell?) or transcriptomics (mRNA sequences; what is the cell transcribing?). This means it becomes possible to understand how metabolic activity of an organism changes as a function of environmental conditions, perturbations, genetic manipulations, etc. Moreover, advanced versions of this technique now allow these analyses in highly complex microbial communities of 1000s of organisms, and even evaluating which proteins are newly expressed using stable isotope tracing.

To prepare microbial samples for proteomic analysis, proteins first have to be extracted and purified from cell material. This is done by first lysing the cell (i.e., disrupting the cell membrane and releasing the cell's contents), followed by chemical steps to remove DNA, RNA, lipds, and other non-protein cellular materials. After obtaining a protein extract, the amount of protein can be quantified using a standardized method (i.e., bicinchoninic acid assay, or BCA assay).

To prepare samples for proteomic analysis, further steps are needed, but will not be covered in this protocol. Specifically, samples have to be digested into shorter peptides, followed by a clean-up step to ensure only peptides are present in the sample before analysis. Proteomic analysis is done on advanced LC-MS devices, allowing highly precise peptide characterization.


## Protocol

### Protein Extraction
* Take a culture grown overnight on liquid medium
* Centrifuge 2 mL of culture at 15,000 g for 4 minutes
* Remove supernatant
* In the fume hood:
    * Resuspend pelleted biomass in 300 µL H<sub>2</sub>O & 1200 µL of ice cold 2:1 chloroform:methanol mixture
    * Vortex for 30 s, rest on ice for 5 min, vortex for 30 s
    * Centrifuge at 15,000 g for 5 min at 4°C. This will result in a phase separation of water/methanol (top) & chlorofom (bottom).
    * Remove the upper layer with a glass pasteur pipette. Be careful to not disrupt the interface layer, this is where the proteins are.
    * Add 1 mL cold 100% methanol
    * Vortex
    * Centrifuge at 15,000 g for 5 min at 4°C
    * Remove as much as possible of the solvent, while minimizing disturbance of the pellet. The more solvent you remove, the faster the next step can go.
    * Dry the sample in the fume hood by letting chloroform/methanol-mixture evaporate
* Once the sample is dried, add 200 µL of the protein resuspension buffer (100 mM HEPES, 8 M urea).

### Protein Quantification
Protein concentration in the sample will be quantified using the bicinchoninic acid assay, which is commercially available as an analysis kit.

* Take 3 1.5 mL Eppendorf tubes
* Add 25 µL of sample to each tube
* Add 200 µL of BCA-reagent to each tube
* Incubate for 30 minutes at 37°C
* Dilute 200 µL of reagent in 800 µL H<sub>2</sub>O
* Analyse optical density at 562 nm
* Calculate average protein concentration & standard deviation using calibration curve provided

## References

Gao et al. (2020). High-Throughput Large-Scale Targeted Proteomics Assays for Quantifying Pathway Proteins in Pseudomonas putida KT2440. Front. Bioeng. Biotechnol. 8:603488. doi: 10.3389/fbioe.2020.603488
