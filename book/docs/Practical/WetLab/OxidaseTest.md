# Oxidase Test

## Introduction

In the electron transport chain during aerobic respiration, oxidase enzymes play an important role in ATP synthesis. Cytochrome oxidase couple oxidation of the electron donor cytochrome c (an Fe-containing hemeprotein) to reduction of the electron acceptor O<sub>2</sub> to H<sub>2</sub>O (Figure 1). To test whether bacteria produce cytochrome oxidase, tetramethyl-p-phenylenediamine dihydrochloride is used as a substitute electron acceptor. Upon reduction by cytochrome c oxidase, this compound produces a purple color. If no cytochrome oxidase is present, the test will remain colourless. 

All microbes that are oxidase-positive can perform aerobic respiration and use O<sub>2</sub> as an electron acceptor. However, this does not necessarily mean that these organisms are obligate aerobes. On the other hand, oxidase-negative bacteria may be anaerobes, facultative anaerobes or just aerobes, as other oxidases might be used in the electron transport chain during aerobic respiration. Consequently, this test only evaluates whether an organism produces cytochrome oxidase, and does not determine the oxygen preferences.

![](OxidaseTest.png)

*Figure 1: The reaction of cytochrome c oxidase in living bacteria (upper panel) and in the oxidase test (lower panel)*

## Protocol

* Take a disc out of the test kit. These discs contains N,N-dimethyl-p-phenylenediamine oxalate and α-naphthol.
* Take an inoculating loop or a toothpick. 
* Touch and spread a well isolated colony on an oxidase disc. 
* The reaction is observed within 2 minutes. 

## References

Collins, C. H. (1967). *Microbiological methods*. Microbiological methods., (2nd Edition).

Acharya, T. (2012). Oxidase test: *Principle Procedure and oxidase positive organisms*. Microbe Online
