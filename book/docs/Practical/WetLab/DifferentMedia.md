# Growth in different media

## Introduction 

Bacteria require macro elements (i.e., C, O, H, N, S, P, etc.) and a range of other (micro)nutrients, such as Mg, Na, Fe, Ni & Se. However, the chemical form of these elements can affect bacterial growth. For example, some bacteria can use N<sub>2</sub> as a nitrogen source, while others need NO<sub>3</sub><sup>-</sup>. Similarly, while some microbes can produce their own amino acids & vitamins, others require it from their environment, or from their growth medium. Consequently, not all bacteria can grow in the same culture medium, and, one microbe may have preferences for other types of culture media.

Culture media can be broadly classified into two groups: complex and defined media. Defined media consist of pure chemicals - e.g., salts, carbohydrates, amino acids, & vitamins. These media offer the advantage of knowing exactly what goes in, and can be modified to the specific needs of the species. Complex media contain components that are not chemically defined, e.g., yeast extract, beef extract, or other undefined mixtures. These mixtures often contain a broad range of growth factors, amino acids, and other components that are needed for growth. While this chemical complexity may not be desirable, it often results in better, faster growth. Moreover, for many microbes, complex media are the only option, as we do not sufficiently understand those organisms to design minimal media for their growth.

Growth media - defined or complex - can be made solid or liquid. In solid media, agar is used as solidifying agent. On these media, bacteria form colonies; a mass of bacterial cells resulting from a single bacterium repeatedly multiplying on a solid surface. For this reason, solid plate media are often used to count the number of bacterial cells in a sample. The morphology of these colonies (e.g., shape, texture or color, see Figure 1) often differs between species, or even between different media for a single species. Alternatively, bacteria can be grown in liquid media, which offer easier access to nutrients, and more homogeneous conditions. A key advantage of liquid media is that it allows to quantify the amount of growth of an organism on the provided nutrients.

![](ColonyCharacteristics.png)
*Figure 1: Examples of colony characteristics*

This experiment compares growth in different media, both solid and liquid. On solid medium, the growth and colony morphology of the bacteria will be observed qualitatively, while liquid media will be used to assess growth quantitatively. 

Each liquid culture will be started with an optical density (OD) at 600 nm of 0.05. After 24 or 48 hours, the optical density will be measured again to determine whether and how much the bacteria did grow in each medium. In this way, the optimal growth medium for each bacterial species can be determined.

## Protocol

### Composition of media used for this experiment

**LB medium:** tryptone, yeast extract, sodium chloride

**CB medium:** casein, peptone, yeast extract, sodium chloride

**BG medium:** beef extract

**MM medium:** glucose, yeast extract, ammonium chloride, magnesium sulfate, potassium hydrogen phosphate, potassium dihydrogen phosphate, spore solution (EDTA, ZnSO4, CaCl2, MnCl2, FeSO4, (NH4)6Mo7O24, CuSO4, CoCl2).

**Mar (Marine Broth):**  Yeast Extract, Ferric Citrate, Sodium Chloride, Magnesium Chloride, Sodium Sulfate, Calcium Chloride, Potassium Chloride, Sodium Bicarbonate, Potassium Bromide, Strontium Chloride, Boric Acid, Sodium Silicate, Sodium Fluoride, Ammonium Nitrate, Disodium Phosphate.

**LBA-, CBA-, BGA-, MMA and MarA medium:** same as above, but with addition of agar for solidification of medium.

### Day 1: Set up experiment

**Solid media: LBA, CBA, BGA, MMA & MarA media**

* Using a sterile inoculation loop, select a single colony from a solid medium plate.
* Spread the bacteria with the zig-zag method on the surface of the fresh plates (one colony per plate).
* Incubate the plates overnight (O/N) at 30°C.

**Liquid media**

Make an overnight culture on LB media:
* Pick a single colony and resuspend it in 5 ml LB media in a culture tube. 
* Culture one night at 30°C.

### Day 2: Observe solid media experiment, set up liquid media experiment

**Solid media: LBA, CBA, BGA, MMA & MarA media**
* Check for presence & morphology of colonies. If no growth is observed, plates can be left to incubate another night.

For morphological terminology, see Figure 1 and list below:
* Shape: Round, irregular, filamentous, rhizoid. 
* Margin: Smooth, rhizoid, irregular, lobate, filamentous.
* Elevation: Convex, umbonate, plateau, flat, raised, raised (spreading edge), flat (raised 
margin), growth into medium. 
* Texture: Moist, mucoid, dry. 
* Appearance: Color, opacity, shine. 

**Liquid media: LB, CB, BG, MM & Mar media**

* Measure the optical density of your overnight culture at 600 nm (OD<sub>600</sub>)
	* Dilute 100 µL of culture with 900 µL of water
	* Mix well
	* Measure OD<sub>600</sub> with the spectrophotometer
	* Multiply this value by 10 to obtain the OD<sub>600</sub> of your overnight culture

* Inoculate each medium to an OD<sub>600</sub> of 0.05.
	* Calculate how much your culture needs to be diluted to obtain OD<sub>600</sub> 0.05
	* For instance, if OD<sub>600</sub> is 2.7, then you should dilute 2.7/0.05=54-fold. If your initial medium volume is 20 mL, you should add 370 µL of culture. 

* Incubate the bacterial culture overnight at 30°C at 250 RPM.

### Day 3: Observe results

* Measure the OD<sub>600</sub> of your bacterium on each medium type. 
* **Tip**: if OD<sub>600</sub> is above 1.0, dilute your culture 10-fold & measure again. Don't forget to multiply the measured value with 10 for correct cell density in this case.

## References

Scythes, K. D., Louie, M., & Simor, A. E. (1996). Evaluation of nutritive capacities of 10 broth media. Journal of clinical microbiology, 34(7), 1804-1807.
