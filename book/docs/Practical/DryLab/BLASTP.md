# BLASTP

As there are many different protein functions available it is crucial to identify the right function for your proteins. In the previous approach you have used BLAST to find similar DNA sequences but you can also use BLAST to find protein sequences in a database. This allows you to identify possible function(s) for a given set of proteins. BLASTP uses a more sophisticated lookup mechanism in contrast to BLASTN where you can only have a match/mismatch or a gap. With BLASTP you can have mismatches that are biochemically nearly the same or very different. Using scoring schemes variations between amino acid hits can give positive or negative scores and based on that homology is inferred.

Beside comparing protein sequences you can also search for functional patterns inside a protein sequence. This is a very sensitive aproach and might give you a more specific function of a protein of interest. 

**Course material:**

* Practical Bioinformatics 2012. Chapter 4
* Practical Bioinformatics 2012. Chapter 8.1 - 8.3
* Presenation on BLASTP
* Read A Critical Guide to BLAST [https://doi.org/10.7490/f1000research.1116052.1](https://doi.org/10.7490/f1000research.1116052.1)

**Key questions:**

* What type of BLAST is used for comparing protein sequences against a protein database?
* Which BLOSUM matrix is preferred when performing a BLAST?
* Why are patterns more sensitive than BLAST?
* Name two different similarity matrices
* What is degeneracy?
* Why does tryptophane have such a high similarity score with itself but not with others?

## Practical

Read and do the exercises in chapter 4 from the book Practical Bioinformatics by Michael Agostino.

After the exercises we are going to look into creating your own BLAST database and use this database to characterise the proteins of your organism.

### Searching a protein domain

Given the following sequence:

```
>myFavorite protein with some function
MKVDIDTSDKLYADAWLGFKGTDWKNEINVRDFIQHNYTPYEGDESFLAEATPATTELWE
KVMEGIRIENATHAPVDFDTNIATTITAHDAGYINQPLEKIVGLQTDAPLKRALHPFGGI
NMIKSSFHAYGREMDSEFEYLFTDLRKTHNQGVFDVYSPDMLRCRKSGVLTGLPDGYGRG
RIIGDYRRVALYGISYLVRERELQFADLQSRLEKGEDLEATIRLREELAEHRHALLQIQE
MAAKYGFDISRPAQNAQEAVQWLYFAYLAAVKSQNGGAMSLGRTASFLDIYIERDFKAGV
LNEQQAQELIDHFIMKIRMVRFLRTPEFDSLFSGDPIWATEVIGGMGLDGRTLVTKNSFR
YLHTLHTMGPAPEPNLTILWSEELPIAFKKYAAQVSIVTSSLQYENDDLMRTDFNSDDYA
IACCVSPMVIGKQMQFFGARANLAKTLLYAINGGVDEKLKIQVGPKTAPLMDDVLDYDKV
MDSLDHFMDWLAVQYISALNIIHYMHDKYSYEASLMALHDRDVYRTMACGIAGLSVATDS
LSAIKYARVKPIRDENGLAVDFEIDGEYPQYGNNDERVDSIACDLVERFMKKIKALPTYR
NAVPTQSILTITSNVVYGQKTGNTPDGRRAGTPFAPGANPMHGRDRKGAVASLTSVAKLP
FTYAKDGISYTFSIVPAALGKEDPVRKTNLVGLLDGYFHHEADVEGGQHLNVNVMNREML
LDAIEHPEKYPNLTIRVSGYAVRFNALTREQQQDVISRTFTQAL
```

Go to [https://www.ebi.ac.uk/interpro/search/sequence-search](https://www.ebi.ac.uk/interpro/search/sequence-search) and put your sequence in there. With the advanced option you can enable or disable protein domain database and thereby speed-up the search. 

Keep at least PIRSF, TIGRFAM, PFAM-A, Prosite-* enabled...

*Click execute, this could take a few minutes... The results are stored in a unique link...

**1: How many different domain / repeat regions are on this protein?**

**2: How many domains do you find for each of the different databases? PIRSF, PF, PS...**

**3: What could the Enzyme number of this protein be?**


### Making your own BLAST database

First a protein sequence dataset is needed of which you want to create a database of. To achieve this we can download protein sequences from public databases such as the NCBI that was used with BLASTN or from UniProt. 

In this exercise we will use the protein database UniProt to retrieve all curated proteins and / or retrieve sets of proteins which might be very interesting for your protein of interest.

To do so go to [http://www.uniprot.org/](http://www.uniprot.org/) and go to the download section. There you can download a lot of different datasets.

**1:** Can you tell us what the difference is between Swiss-Prot and TrEMBL?

**2: Are all proteins from TrEMBL in Swiss-Prot?**

**3: Are all proteins from Swiss-Prot also in the TrEMBL database?**

**4: How is an entry curated and added to Swiss-Prot?**

In the download screen where you had an overview of all the databases you can download the **Swiss-Prot** or **TrEMBL** sequences in XML / FASTA / Text format. 

Normally you would click the FASTA button of the database that you are interested in and download this to your computer. When all the students are downloading this file at the same time it might take a while to download it to your computer.

To save you time we have placed the FASTA file in the data libraries in Galaxy.

In galaxy load the SwissProt file to your history.

**Check what format the SwissProt file is:** Click on the text of the entry (green box) on the right 

**5: How many sequences are currently in Swiss-Prot?**

**6: Are hypothetical / uncharacterised proteins also in this database? (Use the eye icon to check the first few)**

**7: Would you have expected this?**

To turn this sequence file into a database such that a program can scan your query sequences against this database the following steps are needed:

Search for Diamond makedb, select the Swiss-Prot entry and execute.

It might take a moment to build the database. Once completed you can then use this database through the **Diamond alignment tool**. We use Diamond as it has a better and more efficient database building and searching infrastructure making it much faster than the normal BLAST.

When you click on the **Diamond alignment tool** many options become available.

**8: What kind of sequences do we want to align?**

In the *Input query file in FASTA or FASTQ format* selection you choose the protein sequences that were obtained by the gene prediction you did earlier.

Will you select a reference database from your history or use a built-in index?

**We use one from the history**
And you can then select the right diamond database in case you have made different versions.

**Format of output file** 
BLAST Tabular, this makes reading the results much simpler

By clicking in the box you can add or remove columns. I personally prefer to add *Subject title* so I can easily read the function of the subject that my queries matches.

If you are interested you can play with the gap penalties or the type of scoring matrix used.

**9: If you would like to know how many proteins of your genome have a match to Swiss-Prot, how many target sequences would you select to make counting easier?**

**10: Execute! How many proteins have a hit against swissprot? How many are putative or hypothetical?**

**What kind of proteins are needed for protein synthesis? Can you find some?**

**Bonus...** If you have not yet filtered the low coverage contigs from your assembly and you have tried to predict genes it is most likely that they are at the bottom of the result list (Larger contigs are at the top of the assembly file thus those proteins are also at the top). When you scroll down in the result list can you actually find protein coding genes that might not be from your organism?


## Creating your own subset BLAST database

Sometimes using Swiss-Prot is not good enough but TrEMBL might return many results which you are just not interested in. So, you want to narrow down your search space....

**Go to the uniprot website and use the UniProtKB page to search for specific protein functions (you are free to choose but we will use CAS proteins as an example)**

**11: How can you easily see which proteins are Reviewed and which are not?**

**12: What is the % of reviewed proteins?**

Lets search for *crispr associated* proteins.

**13: How many proteins do you find? And how many reviewed?**

You can select a subset of your search or further filter the results based on popular organisms, or only reviewed or un-reviewed. 

When you do not select any sequence and you click **Download** It will automatically download all the sequences, lets select for Reviewed, and download these sequences as FASTA.

If you downloaded the FASTA as Compressed you first need to unpack it on your computer before uploading this into galaxy.

- Upload the FASTA file into Galaxy.
- Use *Diamond makedb* to create a blast database of your sequences of interest
- Perform a Diamond alignment using this newly created BLAST database and your proteins you would like to analyse.

**When you have found CRISPR repeats: Do you find corresponding CAS proteins that are needed for this machinery to work?**

**When you have not found CRISPR repeats: Do you also not find these proteins or do you happen to have acceptable hits? And if so, how could this be?**

