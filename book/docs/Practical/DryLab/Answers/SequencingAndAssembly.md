# Sequencing and Assembly

## Formats

**Q1:** Fasta format: In your own words. What is Fasta format? Can you describe the format?

**The fasta format is a sequence format that starts with a >HEADER and is followed on the next (multiple) lines by a sequence for example:**

	>HEADER extra information about this sequence
	ATGCATGCATGCATGCATGCATGCATGCATGCATGCATGCATGCATGC
	ATGCATGCATGCATGCATGCATGCATGCATGCATGCATGCATGCATGC
	ATGCATGCATGCATGCATGCATGCATGCATGCATGCATGCATGCATGC
	ATGCATGCATGCATGCATGCATGCATGCATGCATGCATGCATGCATGC

**Q2:** What is the difference between Fasta and Fastq format? Can you describe the Fastq format and point out the differences between Fasta and Fastq

**The fasta format is described above. A FASTQ format is a sequence file format that is obtained from the sequence machine. **

**From wikipedia:**

	@SEQ_ID
	GATTTGGGGTTCAAAGCAGTATCGATCAAATAGTAAATCCATTTGTTCAACTCACAGTTT
	+
	!''*((((***+))%%%++)(%%%%).1***-+*''))**55CCF>>>>>>CCCCCCC65


* Line 1 begins with a '@' character and is followed by a sequence identifier and an optional description (like a FASTA title line).
* Line 2 is the raw sequence letters.
* Line 3 begins with a '+' character and is optionally followed by the same sequence identifier (and any description) 
* Line 4 encodes the quality values for the sequence in Line 2, and must contain the same number of symbols as letters in the sequence.


## Coverage 

**Q3:** How many lines do the file(s) have? **TIP**: You can use the tool in Galaxy (on left side in the browser)“Line/Word/Character count”

    39800 lines for  bit_50cov_R1.fastq
    39800 lines for bit_50cov_R2.fastq
    4776 lines for bit_6cov_R1.fastq
    4776 lines for bit_6cov_R2.fastq


**Q4:** How many reads are in the file(s)? **(Remember how many lines 1 fastq has)**

**Devide by 4 as there are 4 information lines per read in a FASTQ file**

**Q5:** What is the read length? **(Click on the eye icon to view the first part of a file)**

**The read length is 150 bp**

**Q6:** How many bases are therefore sequenced?

**150 base pairs \* the number of reads = 2388 * 150 = 358.200 bases**
**Since we have paired-end reads you have to take into account the reads from both files**

**Q7:** How many contigs are to be expected for the 6x coverage and 60kb genome size?

**N e * (-NL / G) = 2388 e (-2388 * 150 / 60.000) = 6**

**Q8:** How many contigs are to be expected for the 50x coverage and 60kb genome size?

**Ne * (-NL / G) = 19900 e (-19900 * 150 / 60.000) = 4.9E-18 (0.0000....) With such a small value we would expect one contig from the assembly**

## Assembly

For the Assembly 6x coverage results answer the following questions:

**Q9:**  How many contigs do you have?

	#name	length	coverage
	NODE_4	9855	3.49745
	NODE_3	13653	3.90962
	NODE_2	15535	3.86221
	NODE_1	20176	3.66592
	
**We have in total 4 contigs of length varying from 9855 up to 20176bp**

**Q10:** Is this according to your expectation? (explain)

**We expected 6 contigs, as this is a mathematical solution it can only take biology into account up to a certain point. Some genomes might have many repeats generating more contigs than expected while other genomes with fewer repeats might perform better than expected.**

**Q11:** N50 is metric for the quality of an assembly - What does the term N50 mean?

**In computational biology, N50 and L50 are statistics of a set of contig or scaffold lengths. The N50 is similar to a mean or median of lengths, but has greater weight given to the longer contigs. It is used widely in genome assembly, especially in reference to contig lengths within a draft assembly.**

**It gives you an idea on the overall quality of an assembly. A higher N50 usually indicates a better assembly.**

**Q12:** What is the N50 of the 6x coverage assembly?


**Q13:** How many contigs do you have?

	#name	length	coverage
	NODE_1	59796	31.8238

**Q14:** Is this according to your expectation? (explain)

**With the formula used we expect 4.9E-18 contigs. As you do not expect 0 contigs after assembly... This would mean that you expect a single contig**

**Q15:** What is the N50 of the 50x coverage assembly?

**As there is only 1 contig the N50 is equal to the length of this contig**

**Q16:** How many bases were assembled in total after filtering for these small contigs?

**This question might have given some confusion and will be changed in the next iteration of the reader. As there are no small contigs in this assembly you can use the length of the single contig**

**Q17:** What is the real coverage?

**The real coverage is given in the FASTA header using the cov part. In this case the coverage is ±31. This is significantly lower than the 50x coverage we used before. Most likely not all the reads were usable for the assembler and some were discarded.**

**Q18:** Why are we not so much interested in contigs shorter than 500bp when attempting to uncover the genetic basis of this genome?

**Contigs that are very short < 500bp or < 1000bp very often do not contain a gene or other features of interest. It is more likely that such a contig originates just from a feq reads indicating by the small coverage size. If you would compare such a small sequence with already known sequences you might find human dna which could have originated from lab work, the sequencing operator or just from skin cells floating in the air.**


## Strain analysis
Above, you have assembled a small 60 kb contig using the SPADES assembler. The process of assembling a real bacterial genome is exactly the same. The only differences is that it requires more computational power and therefore takes longer from a few mintutes to multiple days if not weeks! The strain that you have been working with in the lab has therefore already been assembled for you using the settings given before.

**To obtain your data, in galaxy click on Shared Data > Data Libraries > DNAseq** 

There you can download the `<yournumber>_1.fq` and `<yournumber>_2.fq` files by selecting the white box in front. After selection click on the `to History button`.

**We assembled your genome using Illumina paired–end reads so do not run SPADES on this data set as it will consume quite some resources and will cost a considerable amount of time**

**Q19:** What are read-pairs? What additional information can be extracted from a read-pair?

**Read pairs are reads that originates from a fragment where the begin and the end of a fragment is sequenced. Often a fragment is around 2000bp long and the reads are 150bp. This would then indicate that the left read is 1700bp apart from the other read sequenced on the other end.**

**Q20:** What is the expected coverage? **Note we are using read pairs here!** 

**For each data set the expected coverage varies. For genome size we could use 5.000.000 bp or variations thereof as this is the size of an average bacterium (varying per species).**

**Q21:** How many contigs do you expect? Is this in expectation with the number of contigs obtained in the given Fasta file?

**Same here... Use the formula from the previous exercises. You could encounter more contigs than expected. This could be due to the fact that your organism has many transposons or repeated regions in the genome making assembly more difficult.**

**Q22:** We have selected a single contig in Fasta format. What could this Fasta header mean?

    >NODE_2755_length_94_cov_1.41176
    
**This is a sequence with a length of only 94 basepairs and a coverage of 1.41176 reads. Basically this is a very poorly assembly contig and most likely should be discared for future analysis**

**To obtain the assembled genome click on Shared Data > Data Libraries > Assembly** and select `<yournumber>.fasta` file. After selection click on the `to History button`.

**Q23:** Use the `Select lines that match an expression` function in Galaxy to extract all the lines matching a `>`.
You will end up with a new file only containing the fasta headers. Load the file in excel and split into columns using the `_` variable. 

**Q24:** What is the lowest coverage?

**This can vary but could be around 1.5**

**Q25:** What is the average coverage and is this in line with the expected coverage?

**Q26:** What is the maximum coverage observed?

**The maximum coverage observed can easily be a multitude higher than the average coverage**

**Q27:** What could be the reasons why the maximum coverage is significantly higher than the average or expected coverage?

**The reason for the coverage is so high is most likely due to repetitive regions. If a repeat, repeats itself ±10x the assembly will stack the repeats on top of eachother instead of behind eachother. This is because the assembler does not know how many repeats there are. It is then likely that you will receive a 10 fold higher coverage than average for such contigs.**


## QUAST
Many tools exist to calculate and analyse the genome assembly results. One of these tools is called Quast and can be found in the Galaxy toolbox on the left. Under **Contigs/scaffolds output file** select your genome fasta file and click **execute**.

You will get a **Quast\_report.html** & **Icarus\_Contig\_size\_viewer** & **Quast\_report.text** & **Quast\_report.tsv** & **Quast\_report.txt**

Under **Quast\_report.txt or tsv** you get a nice summary of the total size, largest contig, N50 etc which can be helpful for your report in the end. The **Icarus\_Contig\_size\_viewer** gives you an interactive overview of the contig size distribution (remember to zoom out!) and overall information with interactive plots using the **Quast\_report.html**.
