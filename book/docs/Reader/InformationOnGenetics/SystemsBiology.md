# Systems Biology

The ultimate goal of system biology is to obtain a computer model of a microorganism based on the different –omics technologies. With the model, a prediction can be made on how the microorganism will react to certain circumstances. This can be interesting when the industry, for example, tries to increase the production of a compound by the microorganism. Computer simulations can be made which are much faster and cheaper then testing all possibilities in the lab. 

Another example is to study pathogens which cause various infections. Treatment is getting difficult as more antibiotic resistant strains are being reported. Making a computer model helps to develop new strategies to combat these pathogens in the future. Brock gives an example on this type of research.


**Course material:**

* Brock edition 14. Chapter 6.10, 6 textbox page 229, 18.11 page 618
* Brock edition 15. Chapter 9.12, 9.13


**Key questions:**

After reading the course material you should be able to answer the following key questions. 

1.	Which techniques can be used to map regulatory pathways?
2.	Which techniques can be used to map metabolic pathways?
3.	How can you isolate a single bacterium from a drop of sea water?
