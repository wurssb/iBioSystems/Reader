# Glycerol stock

## Introduction 

Bacteria on an LB agar plate can be stored at 4°C for a few weeks. However, if you want to store bacteria for a longer time, other approaches are needed. A common technique is to store microbes in -80°C in glycerol stocks. The addition of glycerol prevents ice crystal formation formed at -20°C. This limits damage to the cell membranes, and increases survival of cells over longer terms. A glycerol stock of bacteria can be stored stably at -80°C for many years. While a routine technique, not all microbes can be stored at -80°C, and may require alternative long-term storage approaches.

Besides microbial strains, bacterial glycerol stocks are also important for long-term storage of plasmids. Although you can store your plasmid DNA at -20°C, many labs also create bacterial glycerol stocks of their plasmids. This way, when you want to make more plasmid DNA, the plasmid will already be in your desired bacterial strain and you will not need to obtain more competent cells and retransform. 

## Protocol

1.	Make or use an overnight culture of the bacteria

2.	After you have bacterial growth, add 500 μL of the overnight culture to 500 μL of 50% glycerol in a 2 mL screw top tube or cryovial and gently mix.

3.	Freeze the glycerol stock tube at -80°C. The stock is now stable for years, as long as it is kept at -80°C. Subsequent freeze and thaw cycles reduce shelf life

