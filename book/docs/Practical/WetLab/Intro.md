# Intro

The wet-lab experiments are distributed over two weeks (week 1 and week 4). Almost all of them will give you information about the phenotype of your strain. Some of the experiments can be conducted in a short time, some take a few days. In the following section, the experiments are sorted alphabetically. For each experiment the protocol is given, preceded by a short introduction that explains the background or the theory of the experiment. 
 
In week 4 of the course on Wednesday and Thursday you will get the opportunity to repeat any experiment that you would like, to double-check with your bioinformatics results.
 
**There are a few rules that you have to follow if you want to get reliable results:**
 
* *Always* read the Introduction and the complete Protocol before you start
 
* *Whenever* you are in doubt about whatever: ***ask a supervisor***
 
* Label every tube and plate with: 
 	* Group number
 	* Experiment name
	* Strain name
	* Date of inoculation
 
* Record every deviation from the protocol in your lab journal
 
* Record every result in your lab journal
 
* Work safely
