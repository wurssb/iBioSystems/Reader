# Heavy metals

## Introduction 

Soils and groundwater can become contaminated with heavy metals due to pollution and runoff from metal industries. In developing countries, this has led to an increase of heavy metals in the soil and groundwater. These can be toxic to plant and animal life, and may cause health issues in people when using groundwater for irrigation, livestock, or drinking. In particular lead (Pb), chromium (Cr), arsenic (As), zinc (Zn), cadmium (Cd), copper (Cu), mercury (Hg) and nickel (Ni) (Wuana and Okieimen, 2011). Heavy metals can also be natural in origin, as is the case in Bangladesh, where an estimated 45% of drinking water wells is contaminated with naturally occurring high concentrations of arsenic (Das, 2024).

Metal-contaminated waste- and groundwaters are therefore clearly health hazards. Bioremediation attempts to use plants, fungi, or microbes to either remove the metals, or to convert them into a non-harmful version. Such technologies could be applied both at the source of contamination, and in field sites with contaminations. A key challenge for this is finding micro-organisms that can convert toxic metal species into non- or less-harmful counterparts. A first step in this direciton is characterizing the heavy metal tolerance of microbes.

## Protocol

### Day 1: Set up experiment

* Take enough LBA-tubes (15 ml per tube) and put them in an 80°C water bath to melt the agar (check step 3 first to see how many tubes you need).
* After melting agar, keep the tubes warm in a 55°C water bath. This keeps agar molten until you are ready to proceed, and allows you to hold tubes without burning yourself.
* Make a concentration series for each metal at 0, 25, 50, 100, 200, 300 and 400 mg/l. Add appropriate volume of the 10 g/L metal stocks:
    * CuCl<sub>2</sub>: 10 g Cu/l 
    * CoCl<sub>2</sub>, 10 g Co/l
    * MnCl<sub>2</sub>, 10 g Mn/l
* Pour the agar with diluted metal solution in a 9 cm Petri dish. Let the plates cool down to solidify the agar, but maintain sterile conditions.
* Inoculate the plates and incubate overnight at preferred temperature.

### Day 2/3: Observe results

* Observe the growth on the plates.

## References

Narasimhulu K, Rao PS, Vinod AV (2010) Isolation and Identifi cation of Bacterial Strains and Study of their Resistance to Heavy Metals and Antibiotics. J Microbial Biochem Technol 2: 074-076. doi:10.4172/1948-5948.1000027

Das, Manjulika (2024) Arsenic contamination and cancer risk in Bangladesh. The Lancet Oncology 25(5): 538.