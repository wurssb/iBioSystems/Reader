# Hydrogen Sulfide Production Test

## Introduction
Some bacteria can produce sulfide gas (H<sub>2</sub>S) either by (i) using sulfur compounds (e.g., sulfate, sulfite, thiosulfate) as electron acceptor instead of O<sub>2</sub>, or by degradation of the amino acid cysteine during protein degradation (See Figure). The H<sub>2</sub>S-test can be used to evaluate if a microbe can produce sulfide through either of these mechanisms. The medium provides sulfur compounds (specifically, sulfate, thiosulfate, and a complex protein source providing cysteine) and iron (provided as Fe<sup>2+</sup>). If the microbe is capable of producing H<sub>2</sub>S, this will result in formation of black Fe<sub>2</sub>S precipitate. 

![](HydrogenSulfideProductionTest.png)
*Figure: H<sub>2</sub>S production*

## Protocol
### Day 1: Set up experiment
* Pick a colony with a straight wire and inoculate deeply in the tube with Sulphite Indole Motility (SIM) medium. 
* Apply a layer of 1 cm of oil on top of the medium to ensure anaerobic or microaerobic conditions.
* Incubate at 30 or 37°C depending on the optimal temperature for your strain for 24-48 hours. 

### Day 2: Observe results

* **Black precipitate** → produces H<sub>2</sub>S 

* **No black precipitate** → does not produce H<sub>2</sub>S 

## References

Moat, A. G., Foster, J. W., & Spector, M. P. (Eds.). (2003). *Microbial physiology*. John Wiley & Sons.
