# BLASTN

We have now learned how to put the reads into contigs and scaffolds as they arrived from the sequence machine. After that, we looked at how to identify the genetic elements such as genes, tRNAs and rRNAs. 

Most applications can already identify the function for tRNAs and rRNAs using the internal algorithms. For genes however, this is more complicated as thousands of different functions are present within a single organism and comparisons against known genes and its corresponding functions is crucial. Luckily, so called databases exists and can be used to search your sequence of interest, either from a genome or a contig from a given genome. However just searching for the same sequence in the database will often yield no result as sequences mutate over time. To overcome this issue BLAST and many variances thereof has been developed. With BLASTN we can perform a nucleotide sequence lookup against a nucleotide database. This database can be a public database as for example is available from the NCBI or your own specialized database with sequences of interest. Using this similarity comparison we can then quickly identify the function or origin of a given DNA sequence.

**Course material:**

* Practical Bioinformatics 2012. Chapter 3
* Lecture on BLASTN


**Key questions:**

* What are the key principles of BLAST?
* What is the input format for BLAST?
* Which flavour of BLAST do you need to find nucleotide sequences using a nucleotide sequence as input?


<span style="color:white">\newpage</span>

## Practical

You can read and do the exercises in Chapter 3 from the book Pracitcial Bioinformatics by Michael Agostino (Brightspace) to gain more insights into the NCBI.

**Perhaps you can blast the contigs using BLASTN?**

To reduce the chance of time-outs you can change the following settings:

- ~~Instead of NR use Others (Whole-genome shotgun contigs)~~
  - Due to a current issue at the NCBI the Whole genome shotgun contigs is an empty database. Please use the NR again until this issue is resolved.
- Limit by Organism: Bacteria 
	- Or exclude bacteria to see if you find something else
	- Or you can try human to check for human contamination
- Highly similar sequences (megablast)

***Very important:***

- Try smaller contigs first

**Q1:** What kind of target sequences do you find, Genes, Genomes?

**Q2:** And what are they?

**Q3:** Do you have any contigs that are of a completely different organism (e.g. Homo sapiens) or has no hits at all? **These contigs might be a contamination...**

**Q3:** Did you find any CRIPR regions in your genome? Can you perhaps identify what the targets represent? (Think about what kind of blast settings you might want to change)

Next week we will be using more sophisticated techniques to identify strains of the same or other species.

**Do not forget to update your labjournal with all the findings you have!**