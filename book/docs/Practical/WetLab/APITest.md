# API Test

## Introduction
Bacteria and other microbes can grow on multiple carbon substrates. This metabolic flexibility aids their survival in variable settings, such as natural ecosystems, or waste treatment facilities. We can evaluate the range of substrates they utilize both genomically (see [Pathway Databases](https://wurssb.gitlab.io/iBioSystems/Reader/docs/Practical/DryLab/PathwayMapping.html)) and experimentally.
The API® 50 CH test is a simple experimental approach to evaluate bacterial growth on a broad range of carbohydrates and their derivatives. This test consists of a test strip with tiny tubes (or, cupules), which each contain a different carbohydrate substrate (Table 1). Filling each cupule with a bacterial suspension in substrate-free growth medium rehydrates the carbon source. During incubation, fermentation of carbohydrates produces acids that cause pH to drop. We can easily observe this pH change with the pH indicator (changing from red to yellow). The first tube, which does not contain any active ingredient, is used as a negative control. 

## Protocol
### Day 1: Set up API Test
**A. Preparation of the strips**

* Each complete test is made up of 5 strips each containing 10 numbered tubes.
Make sure that you have an incubation box (tray and lid) and a package of strips
* Write your group number and strain number on the elongated flap of the tray.

* Add tap water to the inoculation tray, and hold tray upside down to remove excess water. Only the small dents in the plastic should be filled with water. This helps to create a humid atmosphere inside the incubation box & reduce evaporation from cupules during overnight incubation.

* Remove the 2 long strips (0-19 and 20-39) from the package, separate into 4 smaller strips (0-9, 10-19, 20-29 and 30-39) and place all 4 in numerical order in the incubation tray.

* Take the remaining smaller strip (40-49) out of the packaging and place it in the tray as well.


**B. Preparation of the inoculum**

*1. Make a heavy culture suspension and determine how much of this suspension is needed for inoculation.*

* Add 1ml of sterile water into a sterile tube.

* Pick up all the bacteria from an overnight plate using a swab and transfer them into the tube to make a heavy suspension of the bacteria. 

* Resuspend bacterial colonies well by vortexing.

<span style="color:white">\newpage</span>

*2. Determine the volume of suspension needed to get a turbidity of 2 McFarland units.*

* Add 5ml of water into a 13ml glass tube.

* Incrementally add 50 µL of the bacterial suspension into the glass tube, until the turbidity reaches 2 McFarland. 

* Compare turbidity by eye to the 2 McFarland standard provided in the lab. Make sure to shake the McFarland standard well before use.

* Record the total volume added to this tube (n µL).

<span style="color:white">\newpage</span>

*3. Make a bacterial suspension for the inoculation of API 50 strips.*

* Open an ampule of the API 50 CHB/E medium as follows: 
	* Hold the ampule with one hand in a vertical position (white plastic cap on top). 

	* Press the cap down as far as possible. 

	* Position the tip of your thumb on the striated part of the cap and press forward to 
snap off the top of the ampule. 

	* Carefully remove the cap. 


* Inoculate the ampule by transferring twice the volume of bacterial suspension you recorded in the previous step (i.e., 2n µL). 

**C. Inoculation of the strips**

* Tilt the incubation box slightly upward
* Fill the cupules with the inoculated medium. 
	* Fill the medium up to the lower edge of the cupule opening.
	* **Tip**: Place the tip of the pipette against the side of the cupule to prevent bubbles in the cupule.
	
* Incubate the strips for 24 and 48 hours at the preferred growth temperature of your organism.

<span style="color:white">\newpage</span>

### Day 2: Observe results
When a carbon source is metabolized, the medium will acidify and the red indicator in the medium will change to yellow. Tube number 25 is different: it will change from red to black. Record the colour after 24 and 48 hours of incubation (See Figure).

If a positive result becomes negative at the second reading, only the first reading should be taken into account.

![](API.png)
 
*Figure: API® 50 CH results (left: positive, right: negative).*


Table 1: Overview of the API strip

|Tube     | Test           |  Active ingredients |
|-------- | -------------- |  ----------------   | 
|0        |                |  CONTROL            |  
|1        |    GLY         |  Glycerol           |
|2        |      ERY       |  Erythritol         | 
|3        |   DARA         |  D-arabinose.       |
| 4    | LARA | L-arabinose               |
| 5    | RIB  | D-ribose                  |
| 6    | DXYL | D-xylose                  |
| 7    | LXYL | L-xylose                  |
| 8    | ADO  | D-Adonitol                |
| 9    | MDX  | Methyl-beta-D-xylopyranoside  |
| 10   | GAL  | D-galactose               |
| 11   | GLU  | D-glucose                 |
| 12   | FRU  | D-fructose                |
| 13   | MNE  | D-mannose                 |
| 14   | SBE  | L-sorbose                 |
| 15   | RHA  | L-rhamnose                |
| 16   | DUL  | Dulcitol                  |
| 17   | INO  | Inositol                  |
| 18   | MAN  | D-mannitol                |
| 19   | SOR  | D-sorbitol                |
| 20   | MDM  | Methyl-alpha-D-mannopyranoside |
| 21   | MDG  | Methyl-alpha-D-glucopyranoside |
| 22   | NAG  | N-acetylglucosamine       |
| 23   | AMY  | Amygdalin                 |
| 24   | ARB  | Arbutin                   |
| 25   | ESC  | Esculin ferric citrate    |
| 26   | SAL  | Salicin                   |
| 27   | CEL  | D-cellobiose              |
| 28   | MAL  | D-maltose                 |
| 29   | LAC  | D-lactose (bovine origin) |
| 30   | MEL  | D-melibiose               |
| 31   | SAC  | D-saccharose (sucrose)    |
| 32   | TRE  | D-trehalose               |
| 33   | INU  | Inulin                    |
| 34   | MLZ  | D-melezitose              |
| 35   | RAF  | D-raffinose               |
| 36   | AMD  | Amidon (starch)           |
| 37   | GLYG | Glycogen                  |
| 38   | XLT  | Xylitol                   |
| 39   | GEN  | Gentiobiose               |
| 40   | TUR  | D-turanose                |
| 41   | LYX  | D-lyxose                  |
| 42   | TAG  | D-tagatose                |
| 43   | DFUC | D-fucose                  |
| 44   | LFUC | L-fucose                  |
| 45   | DARL | D-arabitol                |
| 46   | LARL | L-arabitol                |
| 47   | GNT  | Potassium gluconate       |
| 48   | 2KG  | Potassium 2-ketogluconate |
| 49   | 5KG  | Potassium 5-ketogluconate |
