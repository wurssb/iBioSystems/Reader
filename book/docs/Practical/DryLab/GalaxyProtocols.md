# Galaxy Protocols

**A more detailed explanation on how to use the specific modules in Galaxy used during this course.**

:::{Please read}
You do not need to execute any program. This is just to show how galaxy looks like and works... Exercises will follow!
:::

First of all, galaxy is located at [https://galaxy.anunna.wur.nl/](https://galaxy.anunna.wur.nl/) and your account needs to be registred by one of the course administrators before you can access this grid with your WUR credentials.

After the course period your account will be removed and you will no longer have access to your data results. If you still wish to have access to your data you will have to download the results to your machine.

#Galaxy
Galaxy is an open, web-based platform for accessible, reproducible, and transparent computational biomedical research.

![](../../Figures/Galaxy.png) 
*An overview of the galaxy interface. On the left you can search for applications to use. In the center is the application interface and on the right you have your job history.*

## Getting your data

### Gene prediction
As we are currently only working with prokaryotic organisms we use Prodigal for the predictions of genes. To use this program type *prodigal* in he search bar on the left and select the right application. Once you have selected prodigal the center page will change into the input layout for prodigal. 

![](../../Figures/Prodigal.png)


The default settings used by prodigal are good enough for how we are planning to use it. Keep in mind the red box, make sure that you have selected the scaffold sequence file and not a random result from your history

When you would execute the program the following message will appear (**we do not execute the program)**

![](../../Figures/ProdigalResults.png)

*Once the program begins it creates new entries on the right in your history. They start grey, become yellow once it is running and turn green when completed succesfully (or red when failed).*

when finished you can click on the "eye" icon to look at the data in your browser. If you click on the text it will unfold and become larger allowing you to download (disk icon) the dataset. To view details click on the **i** icon so that you can trace back the settings used.


### Other modules
The usage of prodigal has just been explained in the previous section. Other modules such as:

* Aragorn: For the prediction of tRNAs and tmRNAS
* RNAmmer: For the prediction of rRNAs
* CRT: For the prediction of CRISPR repeats

All work according to the same principles. You can find the application on the left (using the search menu saves you time) and select the right input file in the center screen. Your results will appear on the right waiting for interpretation.

Of course there is more than just modules for the prediction of elements. Handy to use modules are ***Line/Word/Character count of a dataset*** or ***Quast QUAST (Quality ASsessment Tool) evaluates genome assemblies*** and even if you want to filter sequences by length there is a module available.

**Now you are familiar with the galaxy interface you can start with the sequencing and assembly exercise**
