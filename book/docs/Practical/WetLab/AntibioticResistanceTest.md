# Antibiotic Resistance Test

## Introduction
One of the greatest scientific and medical achievements in human history is the discovery of antibiotics. Antibiotics are very important in the treatment of bacterial infectious diseases. However, some bacteria are resistant to certain antibiotics. Bacteria can have different mechanisms to counteract the anti-microbial activity of antibiotics. They can for example harbor efflux pumps, modify the target of the antibiotic or inactivate them. Resistance genes can be transferred among bacterial cells which caused the dissemination of numerous antimicrobial-resistances through a diverse range of bacterial species. Although antibiotic resistance can be detrimental, it can also be used in a beneficial way. Resistance genes can be exploited as selective markers in bio-engineering, facilitating the detection and selection of plasmid-borne clones. 

## Protocol
### Day 1: Set up experiment
* Resuspend a single colony in 0.15 ml of sterile water
* Spread the 150 μL suspension evenly on a LB plate with an L-spreader. Allow the liquid to dry on the plate. 
* Gently place the antibiotic disks on top of the plate with bacteria with help of the dispenser. Clean the bottom of the dispenser with 70% EtOH or IPA (isopropanol) before use.
    * **TIP**: If not all antibiotic disks are deposited on your agar, place them manually. For this, there will be antibiotic disk tube available for your use. Use flame-sterilized tweezers to pick the antibiotics disks and deposit on your plate.
* Incubate the plates right side up (with the lid on top) overnight at 30 °C or until cells have grown out completely.

* Antibiotics codes:
    * AMP10: Ampicillin
    * E15: Erythromycin
    * Cn10: Gentamicin
    * P5: Penicillin G
    * RD5: Rifampcin
    * S25: Streptomycin




### Day 2: Observe results
* Observe growth on the plate with antibiotic disks. Measure the clear area surrounding each disk.
* The amount of clear space around each antibiotic disk indicates the lethality of that antibiotic (See Figure). Highly effective antibiotics (disk C) will have a large zone of inhibition surrounding the disk, while an ineffective antibiotic (disk A) will not have a zone of inhibition. 

![](AntibioticResistanceTest.png)
*Figure: Example of antibiotic test results*

## References

Jan Hudzicki, Kirby-Bauer (2016) Disk Diffusion Susceptibility Test Protocol, American Society for Microbiology 2016. Pages 1-23

Zeeshan A. Khan, Mohd F. Siddiqui and Seungkyung Park (2019) Current and Emerging Methods of Antibiotic Susceptibility Testing, Diagnostics 2019, 9(2), 49
