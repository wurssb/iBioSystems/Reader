# Microbial Physiology

There is a distinct set of features which make an strain unique and different than others. We learnt before how the information is stored in the DNA, in this lecture we will focus on the visible part, on the metabolism or physiology of the cells. The physiology of a prokaryote strongly depends on the pathways encoded in its genome. Understanding the pathways can be a powerful tool to be able to interpret correctly the genome, which could lead to interpretation of the physiology of uncultured species for instance.

Building on the measured characteristics in the practical in the laboratory, we review the concepts of temperature range, salinity range, cell wall structure, etc.. Special attention will be given to the substrate utilization. Starting with complex molecules such as proteins, carbohydrates and lipids, cells will first perform an hydrolysis step. Hydrolysis is mediated by enzymes secreted externally and therefore there is no energy gain for the cell. The smaller molecules generated are now transported inside the cells.

Following the route with carbohydrates as model molecules, more specifically glucose, we can flow though the first pathway: glycolysis. There are different pathways (and enzymes) in different microorganisms and this will condition the yield of molecules like ATP and NADH. A typical end product will be pyruvate. At this point two main types of metabolism could be distinguished: respiration (when there is an external electron acceptor) or respiration (when there is not). Under respiratory conditions, pyruvate  (C3) will undergo oxidation until acetyl-CoA and CO2 and then continue to get completely oxidized (2 more CO2) through, for instance, the TCA cycle. Under fermentation, the cells needs to find a redox balance (getting rid of electrons) by producing reduced compounds.

**Course material:**
 
* Brock edition 14. Chapter 2.2, 2.5, 2.11; 2.16, 2.17, 3.8, 3.9, 3.11,  5.6, 5.11, 5.15, 27.17

**Key questions:**
 
After reading the course material you should be able to answer the following key questions. 
 
1. Is cell morphology a good predictor of other properties of the cell?
2. Which are the limitations in the smallest and biggest size of the cells?
3. Why do bacteria need cell walls?
4. What are the main differences in the cell membrane of bacteria and archaea?
5. What is formed when an endospore germinates?