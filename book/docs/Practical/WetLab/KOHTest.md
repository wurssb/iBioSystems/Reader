# KOH Test

## Introduction 

To confirm the results of a Gram stain, the KOH test for Gram determination can be used. In this test potassium hydroxide (KOH) is used to lyse (i.e., disrupt the cell wall & release the cells’ contents) Gram-negative bacterial cells. This will lead to the formation of a very viscous gelatinous mass (Figure 1). Gram-positive cells will stay intact upon contact with KOH, and thus no increase in viscosity will be observed.

![](KOH.png)

*Figure 1: KOH test. A gooey substance is seen, which indicates that the bacterium is Gram- negative*

For reference, here is a video showing the kind of consistency a Gram-negative bacteria would have as a result of this test: <https://www.youtube.com/watch?v=EVBzwyrK6uI> 

## Protocol

* Pipette 50 μl of 3% KOH onto a microscope slide. 

* Add several colonies using an inoculation loop and mix. 

* Immediately observe by the naked eye. 

* **Note:** Gram-indeterminate cells will appear Gram-negative in this test.

## References

Suslow, T.V., Schroth, M.N & Isaka, M., (1981). *Application of a Rapid Method for Gram Differentiation of Plant Pathogenic and Saprophytic Bacteria Without Staining*. Phytopathology 72: 917-918.

Moaledj, K., (1986) *Comparison of Gramstaining and alternate methods, KOH test and aminopeptidase activity in aquatic bacteria: their application to numerical taxonomy*. Journal of Microbiological Methods 5:303—310.
