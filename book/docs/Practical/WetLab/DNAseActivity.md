# DNAse Activity

## Introduction

Deoxyribonuclease (DNase) is an enzyme that breaks down DNA into smaller fragments, and plays a role in many cellular processes. Some microbes can release this DNase extracellularly, allowing breakdown of DNA in their environment. This is a distinguishing characteristic of certain bacteria, although the significance and purpose of DNase production by bacteria has thus far remained largely unclear. Proposed physiological roles for bacterial DNase production include liberation of nucleotides that can subsequently be used for growth, or altering the viscosity of their physical environment to facilitate infection and spread in hosts. Additionally, these extracellular DNases could also affect the mechanical integrity & strength of microbial biofilms, as extracellular DNA is an important component of the extracellular biofilm matrix.

DNase agar is a differential medium that tests the ability of an organism to produce extracellular DNase. This agar contains nutrients for the bacteria, DNA, and methyl green as an indicator. Methyl green is a cation which binds to the negatively-charged DNA. 

![](DNAseActivity.png)

*Figure: An DNAse test plate with two positive strains.*

## Protocol

- Plates with DNA agar
- Controls (positive, negative)

### Day 1: Set up experiment

- Inoculate a DNA agar plate in duplo.
	- Divide the plate in 4 sections
	- Inoculate the sectors with a negative control, positive control, 2 sectors with your own bacteria.
- Put it for O/N in the incubator.    

### Day 2/3: Observe

- Observe the growth on the plates. When the DNA is broken down, it no longer binds to the methyl green, and a clear halo will appear around the areas where the DNase-producing organism has grown.

## References

Atlas, Ronald M. Handbook of Microbiological Media for the Examination of Food. CRC/Taylor & Francis, 2006.
