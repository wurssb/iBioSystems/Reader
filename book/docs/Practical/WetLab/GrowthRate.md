# Growth at different temperatures

## Introduction

Each microorganism has its own temperature preference and sensitivity as a result of the enzymes, ribosomes, membranes, and other components making up the cell. Consequently, growth rates of microorganisms plotted as a function of temperature have distinct cardinal temperatures: minimum, maximum, and optimum. The minimum and maximum growth temperatures are respectively the lowest and highest temperatures that can still support growth, while the optimum temperature is the temperature at which the bacterium grows fastest. 


Bacteria can be classified into five major groups based on their optimal growth temperature; Psychrophiles, psychrotrophs, mesophiles, thermophiles and extreme thermophiles (See Figure 1). The majority of cultured bacteria are mesophilic, i.e. they grow optimally between 18°C and 45°C. 

The objective of this experiment is to classify your bacteria based on their temperature preference for growth. 

![](GrowthRate.png)
*Figure 1: Thermal classification of bacteria*

<span style="color:white">\newpage</span>

## Protocol

### Day 1: Set up experiment
* Make as suspension of a single colony (if possible) in 0.5 ml of sterile water.
* Inoculate 4 LBA plates using an inoculation loop.
    * **TIP**: Inoculate using the Zig-Zag method.
* Incubate the plates for 24-48 hours at 10, 30, 37 and 45°C. 

### Day 2/3: Observe results

* Observe the presence or the absence of bacterial colonies on each plate. 

## References

White, D., Drummond, J. T., & Fuqua, C. (1995). The physiology and biochemistry of prokaryotes.
