# iBioSystems

In this era of genomics, computational analysis has become an integral part of normal 'wet-lab' routines. In this course we introduce the Moist-lab: a new style of research where hands-on wet-lab experiments and computational predictions are combined in an integrative wet-dry cycle to study phenotype-genotype relationships in prokaryotes and simple eukaryotes. In this course we will, by doing wet-lab experiments, determine interesting biochemical and physiological properties from these organisms and they are then compared with genotypic properties that we will predict hands-on from genome data.

During the course a digital lab journal will be used to keep track of the entire process enabling to do your research in FAIR way (Findable, Accessible, Interoperable, Reusable).
Learning outcomes:

After successful completion of this course students are expected to be able to:

* Analyze predictions of commonly used computational tools in the field of genomics;
* Analyze and design Moist-lab experiments;
* Document experimental procedures and store data and data provenance in a FAIR way;
* Communicate research findings through a scientific report.

**Activities:**

This is hands-on group work (wet-lab 2; dry-lab 2 persons). 

Student are expected to: attend lectures; perform several simple laboratory experiments; maintain a digital lab journal; write a digital lab report.
Examination:

The final mark is based on; a written exam that contains a number of multiple-choice questions (one-third on the experimental part, two-third on the computational part) (2/5), a concise digital lab journal (1/5) and a digital lab report (2/5). 

To pass the course, the mark for each part should be at least 5,5.