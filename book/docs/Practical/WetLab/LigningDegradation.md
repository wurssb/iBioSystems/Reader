# Lignin Degradation

## Introduction

Lignin is a complex aromatic polymer that is found in the cell wall of plants. It provides plants with compressive strength and protection from pathogens. Lignin-containing compounds are found in forests (rotting wood) and in the waste effluent of for example the paper industry. Some bacteria and fungi can use lignin as a carbon source. This is very interesting for the bio-based production of chemicals. Bacteria that can metabolise lignin and subsequently produce valuable chemicals can, in other words, turn waste into something useful. 

An easy way to test the ability of bacteria to degrade lignin, several indicator molecules (dyes) can be used. These molecules resemble the complex lignin polymer and will turn from blue to colourless when they are degraded (See Figure).

![](LigningDegradation.png)

*Figure: Degradation of an indicator dye.*

## Protocol

### Day 1: Set up exeperiment

* Put LBA-tubes (15 mL, one for each indicator dye) in an 80°C water bath to melt the agar.
* After melting agar, keep the tubes warm in a 55°C water bath. This keeps agar molten until you are ready to proceed, and allows you to hold tubes without burning yourself.
* Dilute the dye stock solutions to a final concentration of 25 mg/L in the molten agar:
	* Methylene Blue: 2.5 g/L 
	* Azure B: 2.5 g/L 
	* Toluidine Blue: 2.5 g/L
* Pour the agar with diluted metal solution in a 9 cm Petri dish. Let the plates cool down to solidify the agar, but maintain sterile conditions.
* Inoculate the plates: 
	* Divide the plate in 4 sections.
	* Use each section for one strain: negative control strain, positive control strain, and 2 sections with your own bacterium. 
* Incubate overnight at your preferred temperature.

### Day 2/3: Observe results

- Observe growth and colour on the plates.

## References

Bandounas, L., Wierckx, N.J., de Winde, J.H. *et al.* Isolation and characterization of novel bacterial strains exhibiting ligninolytic potential. *BMC Biotechnol* **11,** 94 (2011) doi:10.1186/1472-6750-11-94

