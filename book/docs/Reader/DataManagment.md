# Data Management

We, as scientists, produce more and more data. But it’s getting more and more difficult to keep track of all this data. In this chapter, we will look into data management and show how to process and organise your generated data in an orderly manner.  
> The parallel between your photos and scientific data.
> The first digital consumer cameras entered the market in the mid-90’s. Later camera’s where included in phones. People can take as much photos as they like. Relatively few people print their photos. Why would you? Photos can be stored for free on the PC or shared via internet.

> But most people don’t spend much time organizing their pictures. Folders pile up with large amounts of photos including those badly taken ones. Many photos are lost because of hard-disk crashes or are simply forgotten to be transferred to the new computer or smart phone.
Finding a specific photo on a unorganized storage medium can be a time consuming activity. Spending time and effort in sorting and organizing pictures in a uniform manner will result in a nice collection.
The same applies for scientific data. It should be sorted and organized so it can be passed on and used by other (future) scientist.

## FAIR

Maintaining your data in an orderly fashion will help you doing your research and writing in an effective manner. Correct and consequent data management helps with this. 
Current practise is that data is stored on multiple devices and/or clouds, in multiple file formats, non-uniform naming of the files and in a filing structure only logical to the researcher. Knowledge of where-to-find-what is lost as soon as the student gets the grade and walks out the door. Gigabytes of useful data are lost for future scientists.
Considering the time and money involved this is a waste of time. Good data management prevents this loss of data but, more important, can help you during lab work and the writing process. 

Good data management is done according to the FAIR standard which stands for Findable, Accessible, Interoperable, Reusable. Details are in the provided course material (Wilkinson, et al. 2016). The FAIR standard is the new standard within science. For example, it’s obligatory to include a data management plan (based on the FAIR principles) when you apply for scientific funding like H2020 of the European Union. 

So, how do you apply this into daily life on the lab? In the following section, we will provide you with tools and techniques to comply with the FAIR rules ánd making your research more efficient.

## Lab journal

Most lab workers have a lab journal. It is usually a paper book where they keep track of the experiments they do. However, keeping track of all the generated digital data in a paper lab journal is undoable. That’s why we will use a digital lab journal called eLABJournal. Keeping a digital lab journal gives you a clear overview of your research and will make the writing of a report much easier. Instructions on how to use eLABJournal can be found in the module ‘eLABJournal’ on BrightSpace.
Using a digitial labjournal is obligatory and the final labjournal will be reviewed in the last week of the course by your peers. 
Data management
Finally, you’ll be instructed on how to organize your data files. Instructions can be found on the website of the Data Management Support Hub of Wageningen UR. It’s a very elaborate and detailed site. For this course only 2 web pages are essential for the course. We will provide the links on brightspace in the module ‘Data management’.

Be aware that various lab equipment can have special files. For example, a qPCR machine can have a file with an *.xplt extension. It can only be red using specialized software. Such files must be exported to common files such ad Excel. This enables other researchers to analyse your data without the need for specialized software.

Also important is to back-up your files. eLABJournal is cloud-based so that’s is already back upped for you. 
All the files you create should be stored on your M: drive. This is your personal drive a can be accessed from any WUR computer. Avoid the C: and D: drive which drive of the specific computer you work on. If the computer crashes you will lose all you stored data. 
USB flash drives are for data exchange, not for data storage. You can use MyWorkspace if you want to access your WUR data from your home. Links in the module ‘Data management’.
Good data management is obligatory and it will be part of the review in the last week of the course by your peers.

## Course material:

* Wilkinson, M. D. et al. The FAIR Guiding Principles for scientific data management and stewardship. Sci. Data 3:160018 doi: 10.1038/sdata.2016.18 (2016). 
* Roche DG, Kruuk LEB, Lanfear R, Binning SA (2015) Public Data Archiving in Ecology and Evolution: How Well Are We Doing? PLoS Biol 13(11): e1002295. doi:10.1371/journal.pbio.1002295 
* eLABJournal Module on Brightspace  
* Data management Module on Brightspace

Key questions: 
After reading the course material you should be able to answer the following key questions. 

### For the Wilkinson, et al. 2016 article: 

1. Explain the FAIR concept. Where does the abbreviation stand for? 
2.	What do the authors mean with the concept of “machine actionable”?
3.	After reading the article, who is the weakest link when applying the FAIR principles, explain:

### For the Roche, et al. 2015 article: 

1.	Given an example of so so-called PDA? 
2.	What did the authors notice about the incomplete studies? 
3.	Give two examples of problems that made understanding of the studies difficult. 
4.	What is an effective approach to let more scientist commit to the PDA policy?

### For the websites of the Data Management Support Hub 

1.	What is considered a ‘master file’?
2.	What is a ‘readmefile.txt’ and what do you put in it?
