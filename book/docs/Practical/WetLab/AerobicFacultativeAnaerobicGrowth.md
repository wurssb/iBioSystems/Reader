# Growth and oxygen

## Introduction 

Microorganisms can be separated based on several criteria, one of which is their ability to tolerate oxygen. Some bacteria need oxygen and respire it, whilst other bacteria only survive in aerobic conditions or even die in the presence of it. The different classes of oxygen tolerance are: 

* Aerobes: Need oxygen to grow. 
* Microaerophiles: Aerobes which prefer or need lower concentrations of oxygen. 
* Facultative anaerobes: Able to grow either with or without oxygen. 
* Aerotolerant anaerobes: Can tolerate oxygen, but cannot respire it. 
* Obligate anaerobes: Are inhibited or killed when oxygen is present. 

This experiment examines if the unknown bacterial strains are facultative anaerobes. LB medium, which contains all the necessary constituents for growth, is used. L-cysteine is added to the medium to reduce oxygen dissolved in the medium, creating an anaerobic environment. Furthermore, resazurin, a redox indicator dye, is added. In the presence of oxygen resazurin is oxidized, turning the medium pink. In the absence of oxygen resazurin will remain colorless.

## Protocol

### Day 1: Set up experiment
* Collect plates for each of the strains you will test:
    1) Your unknown strain
    2) *Escherichia coli* as a positive control (facultative anaerobe)
    3) *Pseudomonas putida* as a negative control (aerobe) 
* Resuspend one colony in 30 µL sterile H<sub>2</sub>O in a sterile Eppendorf tube. Each strain goes in a separate Eppendorf tube. 
* Inoculate your suspension in liquid LB-medium.
    * **TIP**: Pipette carefuly to minimize introduction of oxygen into the medium.
* Add 50 µL of resazurin and 100 µL L-cysteine to each of the three test-tubes.
    * **IMPORTANT**: Do not shake the tubes! Shaking will dissolve oxygen in the liquid.
* Add 1 cm of paraffin oil on top of the liquid to make sure that no oxygen can dissolve in the medium anymore.
* Incubate overnight at your preferred temperature.

### Day 2: Observe results
* Compare the growth of your strain with the two controls.
    * **Note**: A pink colour in your medium indicates the medium was not fully anaerobic, and oxygen may have dissolved in your medium.

## References

Kevin L. Anderson and Daniel Y. C. FUNG (1983) Anaerobic Methods, Techniques and Principles for Food Bacteriology: A Review. Journal of Food Protection, Vol. 46, No.9, Pages 811-822 (September 1983)
