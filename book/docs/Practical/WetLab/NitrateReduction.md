# Nitrate Reduction

## Introduction
Anaerobic respiration involves the reduction of inorganic molecules (other than oxygen), by using them as terminal electron acceptors. Some of the most common molecules are inorganic nitrogen compounds, including ammonium (NH<sub>4</sub><sup>+</sup>), nitrogen gas (N<sub>2</sub>), nitrous oxide (N<sub>2</sub>O), nitric oxide (NO), nitrite (NO<sub>2</sub><sup>-</sup>), and nitrate (NO<sub>3</sub><sup>-</sup>). Microbes expressing nitrate reductase, a molybdenum- containing membrane-integrated enzyme, can catalyze the reduction of nitrate to nitrite. Other microorganisms have the ability to further reduce nitrite to nitrogenous gasses, such as NO, N<sub>2</sub>O, and N<sub>2</sub> (See Figure). This process is known as denitrification and is widely used in sewage treatment to remove pollutant nitrogen from wastewater before release in the environment. Besides local environmental impact, denitrification also has global climate impacts, as the intermediate N<sub>2</sub>O is a potent greenhouse gas 298 times stronger than CO<sub>2</sub>. Denitrifying organsims may both release and capture atmospheric N<sub>2</sub>O, although the balance between these processes is not yet well-understood. Last, as an alternative to denitrification, some bacteria may use NO<sub>2</sub><sup>-</sup> as terminal electron acceptor to produce NH<sub>4</sub><sup>+</sup> instead of N<sub>2</sub>. This process is call dissimilatory nitrite reduction to ammonium (DNRA), and is studied for its role in cycling nitrogen without contributing to greenhouse gas formation. 

Nitrate reduction can be detected by culturing bacteria in a nitrate broth under anaerobic conditions. After overnight incubation, the ability of the microorganisms to reduce the provided NO<sub>3</sub><sup>-</sup> to NO<sub>2</sub><sup>-</sup> is measured by the addition of sulfanilic acid (Reagent A) and α-naphtylamine (Reagent B). More specifically, if NO<sub>2</sub><sup>-</sup> is present, sulfanilic acid (Reagent A) forms a colorless nitrate-sulfanilic acid complex, which subsequently reacts with α-naphtylamine (Reagent B) resulting in a cherry-red precipitate (prontosil) (Figure 1). If no color is observed, the organism either does not have the ability to reduce NO<sub>3</sub><sup>-</sup> to NO<sub>2</sub><sup>-</sup> (absence of nitrate reductase activity), or all NO<sub>2</sub><sup>-</sup> was further reduced to NH<sub>4</sub><sup>+</sup> or N<sub>2</sub>. To distinguish between these options, zinc powder is added to chemically reduce NO<sub>3</sub><sup>-</sup> to NO<sub>2</sub><sup>-</sup>. In this case, red color indicates the microorganism cannot reduce NO<sub>3</sub><sup>-</sup> to NO<sub>3</sub><sup>-</sup>, while no change of color indicates NO<sub>3</sub><sup>-</sup> was fully reduced to NH<sub>4</sub><sup>+</sup> or N<sub>2</sub>. 

![](NitrateReduction.png)

*Figure 1: Reduction of nitrogenous compounds. The right side of the shows the nitrate production test*

## Protocol
### Day 1
* Resuspend a colony in 50 µL of sterile water in a sterile eppendorf tube.
* Inoculate 5 mL Nitrate Broth in a glass tube with the cell suspension. Do this slowly and carefully to prevent adding oxygen to the medium.
* Add 1 cm of paraffin oil on the surface of the liquid culture (to create anaerobic conditions).
* Incubate at optimal temperature for your organism for 24 to 48 h. 

### Day 2
* Add 3 drops of Reagent A and 3 drops of Reagent B. Invert tube several times and observe the color change after a few minutes. 
    * **IMPORTANT:** Keep thumb on lid to prevent leakage.

* If the suspension turns pink-red: NO<sub>3</sub><sup>-</sup> was reduced to NO<sub>2</sub><sup>-</sup>. The test is complete.
* If the suspension remains colorless:
    * Add a small amount (“sharp knife point") of zinc powder to the medium. 
        * **TIP**: Addition of too much zinc powder can result in a false-negative reaction. 
    * Shake the tube vigorously and allow it to stand at room temperature for 10-15 min. 
    * If the medium remains colourless: NO<sub>3</sub><sup>-</sup> was completely reduced to NH<sub>4</sub><sup>+</sup> or N<sub>2</sub>
    * If the medium turns pink: NO<sub>3</sub><sup>-</sup> was still present in the tube, no reduction took place.

* Test a negative control by using a fresh Nitrate Broth tube. There should be no pink color formation after adding reagent A and B and if zinc powder is added the color should change to pink. 

## References

Leboffe, M. J., and Pierce, B. E. (2011). *Exercises for the Microbiology Laboratory* (Vol. 4). Englewood, MA: Morton Publishing Company.

Mac Faddin, J. F. (1980). *Biochemical tests for identification of medical bacteria* (Vol. 2). Williams & Wilkins Co.

Knapp, J. S., & Clark, V. L. (1984). Anaerobic growth of Neisseria gonorrhoeae coupled to nitrite

reduction. *Infection and immunity*, *46*(1), 176-181.

Skerman, V. B. D. (1967). A guide to the identification of the genera of bacteria. *The Williams & Wilkins Co.*, 218 – 220.
