# Catalase Test

## Introduction
Catalase - a peroxidase-type enzyme - can convert H<sub>2</sub>O<sub>2</sub> to H<sub>2</sub>O and O<sub>2</sub>. This enzyme protects the cell from the toxic effects of reactive oxygen species that are formed in the presence of O<sub>2</sub>. The catalase test is a quick and easy experiment to evaluate if microbes express catalse. Upon adding microbial cells to a 3% hydrogen peroxide solution, the appearance of bubbles (caused by O<sub>2</sub> formation) in the liquid indicates that catalase is present. Absence of catalase results in a few tiny bubbles or no bubbles at all.  

## Protocol
* Add a drop of 3% H<sub>2</sub>O<sub>2</sub> to a glass slide. 
* Add several colonies using a plastic inoculation loop and mix.
* After 5-10 seconds, observe by the naked eye. 

**Important notes**

* Do not use a metal loop with H<sub>2</sub>O<sub>2</sub>; it will give a false positive result and degrade the metal. 
* A few tiny bubbles forming after 20 to 30 seconds is not considered a positive result because some bacteria possess enzymes other than catalase that can decompose hydrogen peroxide.

## References

Cutler, R. G. (2005). Oxidative stress and aging: catalase is a longevity determinant enzyme. Rejuvenation research, 8(3), 138-140.
