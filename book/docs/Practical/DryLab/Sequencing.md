# Sequencing

Understanding the complexity of an organism often starts with unravelling the order of the nucleotides from a DNA sample. It is important to know that a single DNA sample can contain the DNA from millions of hopefully near identical cells. One of the first widely adopted sequencing technologies was invented by Sanger (1975) and is therefore called Sanger sequencing. It uses a dideoxynucleotide_chain termination stopping the process when such a nucleotide is attached. It was a relatively efficient approach and reduced the radio activity but was very time consuming.

Second and Third-generation sequencing technologies are currently the most widely implemented.  Illumina (second generation) can generate millions of relatively short reads at a very high speed whereas PacBio (third generation) on the other hand can generate fewer but relatively long reads. Each technique has its pros and cons and the complexity of an organism has to be taken into account when a high-quality dataset is to be obtained. Fourth generation sequencing is slowly becoming available of which the Oxford Nanopore is the most interesting as it can generate extremely long reads and can fit inside your pocket.

**Course material:**

* Brock edition 14. Chapter 6.1, 6.2
* Brock edition 15. Chapter 9.1, 9.2
* Lecture on Sequencing

**Key questions:**

1. What would be an approach on sequencing an organism with many repeated regions in the genome?
2. Describe the principles of Sanger sequencing
3. What is shotgun sequencing?
