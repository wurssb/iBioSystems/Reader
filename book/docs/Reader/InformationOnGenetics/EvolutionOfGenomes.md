# Evolution of Genomes

Genes evolve over time by mutations in the sequence of the genome. These mutations have usually a negative impact on a microorganism but sometime the mutation is beneficial is passed on. These mutations give us insight in the evolution of microorganisms and how different species are related to one another. With help of the 16S ribosomal RNA gene we can even order and classify all prokaryotes in the tree of life. We will look at this into detail in later chapters. There are many ways in which mutations can take place within the genome:

Both prokaryotes and eukaryotes can have multiple copies of the same gene on their genome. If such a duplication occurs a copied segment of sequence can mutate. The original sequence won’t mutate and keeps it original function. Even if the copy mutates it has still large similarities with the original sequence. We call these homologs. Homologs can be sub-divided into orthologs and paralogs. The definition is a under debate. Although the difference is explained in Brock we rather refer to the paper of Jensen (2001) in which he explains the difference between orthologs and paralogs. Focus on Figure 1c and note that orthologs are duplicates trough a speciation event. All other events lead to paralogs. 

There can also be a deletion of segments of sequence. This can occur when a species changes it lifestyle rendering some genes obsolete.

Genes are passed on through generations and this is called vertical gene transfer. But under some circumstances microorganisms can pass on genes. Even between different species. How this is done and under which circumstances this happens can be found in Brock.

All microorganisms from a species, such as Escherichia, share the same genes. This is called the core-genome. But Escherichia coli and Escherichia hermannii have different genes besides the core genome. This cluster of sub-species-specific genes is called the pan-genome. How microorganisms obtain these extra genes is explained in Brock.

**Course material:**

* Brock edition 14. Chapter 6.11, 6.12, 6.13
* Brock edition 15. Chapter 9.5, 9.6, 9.7
* Paper: Jensen R.A. (2001), Orthologs and paralogs – we need to get it right. Gene Biology 2 (8).

**Key questions:**

After reading the course material you should be able to answer the following key questions. 

1.	What are gene families?
2.	Try to explain in simple language the differences between orthologs and paralogs.
3.	Name three ways of horizontal gene transfers.
4.	Under which circumstances does horizontal gene transfer occur.
5.	Explain in a couple of sentences what chromosomal islands are and how they end up in a microorganism.
