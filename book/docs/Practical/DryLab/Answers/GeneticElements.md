# Genetic Elements
After sequencing and assembly, the next step is annotating the assembled parts. There are many features that can be identified on a given genome of which the majority is mostly the protein-coding genes. Other features such as transfer RNAs and ribosomal RNAs are nearly always present and are responsible for the synthesis of the protein-coding genes into proteins. Other features such as binding sites, repeat regions, mobile genetic elements or CRISPRs can also be computationally predicted. 

In this course, we mainly focus on the three elements required for protein synthesis, the gene, rRNAs and the tRNAs and how these elements are predicted. There are currently numerous tools available for the identification of these genetic elements. Some of these applications are specialized for eukaryotes while others are available for prokaryotes. Even within a single organism, different applications will yield different results as none of them are perfect. It is therefore crucial to keep in mind that a missing gene might still be there or a predicted gene might be non-functional due to mutations hidden within the sequence.

**Course material:**

* Brock edition 14. Chapter 6.3
* Brock edition 15. Chapter 9.2
* Practical Bioinformatics 2012. 1.4, 1.5
* Lecture on genetic elements


**Key questions:**

1. Describe the basic steps in finding a gene
2. Explain why it is most likely you will not find tRNAs for all codons in a given genome
3. In which group is the gene density higher? Prokaryotes or eukaryotes?

<span style="color:white">\newpage</span>

## Practical

In the previous exercise we have looked at how assembly works and how to interpret quality (N50, coverage). In this exercise we are going to find the genetic elements that makes the organism work. The majority of the organisms genetic elements are encoding for genes that in turn encode for proteins. Other elements also exist.

Q1: What kind of genetic elements do you know? Name at least 3...

* tRNA
* tmRNA
* rRNA
* Protein coding genes

Q2: And what are their functions?

**A transfer RNA (abbreviated tRNA and formerly referred to as sRNA, for soluble RNA) is an adaptor molecule composed of RNA, typically 76 to 90 nucleotides in length, that serves as the physical link between the mRNA and the amino acid sequence of proteins.**

**A transfer messenger RNA (tmRNA) is a ribosome rescue system. When a ribosome is stuck during translation the tmRNA jumps in on the A-site of a tRNA. the tmRNA will take the position of the tRNA and places 10 Alanines on the peptide chain as a signal for proteases. When the peptide is them removed from the ribosome it is detected and degraded by proteases.**

**Ribosomal ribonucleic acid (rRNA) is the RNA component of the ribosome, and is essential for protein synthesis in all living organisms.**

**Protein coding genes are genetic elements that are translated to mRNAs and eventually to proteins through the translation machinery**

Q3: Bacterial genes are on average 900 basepairs long. What is then the average length of a protein?

**3 nucleotides encode for 1 amino acid. Thus on average a protein is 300 AA long.**

Q4: Given a bacterial genome size of 1.000.000 bp how many genes would you expect?

**You would expect around 1100 genes. Most likely this number will be lower for a given organism as you have to take other elements into account.**

Q5: Given your organism, remember the assembly size from the previous exercise, how many genes do you expect?

### Predicting genes

Q6: Looking at the Protein translations file. How many genes have been found?

Q7: Is this within your expected range? If not, ca you think of why not?

### Predicting tRNAs

Q8: What is the function of a tRNA?

**A transfer RNA (abbreviated tRNA and formerly referred to as sRNA, for soluble RNA) is an adaptor molecule composed of RNA, typically 76 to 90 nucleotides in length, that serves as the physical link between the mRNA and the amino acid sequence of proteins.**

Q9: Is the tRNA a protein or something else?

**RNA molecule**

Q10: How many tRNAs do you expect to be in your genome?

**You could expect one tRNA for each codon in the genome. However, some genomes have a strong preference for certain tRNAs and have therefor multiple copies of this tRNA to ensure expression. Other genomes might miss certain tRNAs and thos codons are translated using the Wobble Effect.**

Q11: Do you expect to find a tRNA for each codon?

Q12: How many tRNAs do you find?

Q13: Did you expect to find more or less tRNAs?

Q14: Are all codons you expected to be covered by a tRNA found?

### Predicting rRNAs

Q15: What is an rRNA?

**Ribosomal ribonucleic acid (rRNA) is the RNA component of the ribosome, and is essential for protein synthesis in all living organisms.**

Q16: Which rRNAs do you expect to find for your organism?

**For prokaryotes you expect to find the 5S, 23S and 16S**

Q17: Which and how many rRNAs did you find?

**Note: If you did not find all the rRNAs that practically should be there, then there are several reasons why this could be the case. 1) The randomization process of fragmenting your genome into pieces might miss highly condense regions where the DNA is coiled and difficult to break. The assembler might have had problems with the supplied reads to put this particular rRNA region into place and you can analyse this further by looking at the positions of the predicted rRNAs on your genome and the size of the contig they are located on.**


### Predicting CRISPR

Q18: What is CRISPR in general?

**“CRISPR” (pronounced “crisper”) stands for Clustered Regularly Interspaced Short Palindromic Repeats, which are the hallmark of a bacterial defense system that forms the basis for CRISPR-Cas9 genome editing technology.**

Q19: What are the CRISPR repeats that are in the genome?

**The region in which these repeats are located upon are essentially the memory bank of incoming nucleic acid sequences used for surveillance against foreign DNA. The repeats flank the pieces of foreign DNA making the organism recognize its memory bank. Without it the bacteria might accidently target itself and degrade its own DNA.**


Q20: What is function of the sequence in between the repeats?

**The so called spacer sequences correspond to pieces of forgeign DNA hat have previously invaded the cell. Once the spacers are recombined into the CRISPR region, the system provides resistance to any incoming DNA that contains the same or very closely related sequences to the individual spacer regions.**


Use the following example if your genome does not have a CRISPR system (or is not found).

| 1                                    | 2       |
|--------------------------------------|---------|
| NODE_1_length_1246052_cov_357.753205 | 2831709 |
| NODE_1_length_1246052_cov_357.753205 | 2831751 |
| NODE_1_length_1246052_cov_357.753205 | 3298031 |
| NODE_1_length_1246052_cov_357.753205 | 3298079 |
| NODE_1_length_1246052_cov_357.753205 | 3298139 |

| 3                    | 4                                        |
|----------------------|------------------------------------------|
| CCAGTGACTCCGAGTCACC  | GGGTAAGGAGGCGATCAACCCAA                  |
| CCAGTGACTCCGAGTCACC  | GGCCAGTGACAAAACGATCAGTGAC                |
| AAGCCTGCGGACGAGAAGCC | GGCGGACGAGAATCCCGTCACGGACGAG             |
| AAGCCTGCGGACGAGAAGCC | GGCCGATGAGAAGCCGGCCGACGACGAGCCCGTCGAGGAC |
| AAGCCTGCGGACGAGAAGCC | TGCGGACGAGAAGCCTGCCGACGAGAAGCCGGCCGACGAG |

| 5  | 6  |
|----|----|
| 19 | 23 |
| 19 | 25 |
| 20 | 28 |
| 20 | 40 |
| 20 | 40 |

Q21: How many different CRISPR repeats are found?

**In the example a total of 5 repeats are found. However there are 2 differen repeats (column 3).**

Q22: How many different CRISPR target sequences are found?

**In total 5 different targets are present (column 4)**

*Later we will learn how to compare sequences against a database. Perhaps you can then identify what the sequences are targeting?*
