# Genetic Elements

In the previous exercise we have looked at how assembly works and how to interpret quality (N50, coverage). In this exercise we are going to find the genetic elements that make the organism work. The majority of the organisms' genetic elements encode for genes that in turn encode for proteins. Other elements also exist.

**Q1:** What kind of genetic elements do you know? Name at least 3...

* ...
* ...
* ...

**Q2:** And what are their functions?

**Q3:** Bacterial genes are on average 900 basepairs long. What is then the average length of a protein?

**Q4:** Given a bacterial genome size of 1.000.000 bp how many genes would you expect?

**Q5:** Given your organism, and remembering the assembly size from the previous exercise, how many genes do you expect?

## Predicting genes

To find the genes in your organism we use a program called prodigal. In galaxy select the program from the Tools section (left). Select the right input file \<ID\>_scaffolds.fasta and click on execute

4 files are begin generated; 

* Protein translations: Containing the protein sequences in FASTA format
* Nucleotide sequences of genes: Containing the sequences of the predicted genes
* Potential genes (with scores): Containing all potential genes (and many false positives) with scoring information
* Prodigal on Data ..: Contains the finally selected genes and the corresponding positions (GFF format)

**Q6:** Looking at the Protein translations file. How many genes have been found?

**Q7:** Is this within your expected range? If not, can you think of why not?

**Q8:** If you had many small contigs did you find any genes on them?

**Q9:** What is the lowest confidence value of the predicted genes?

**Q10:** How many genes have a confidence value > 95%


## Predicting tRNAs

**Q11:** What is the function of a tRNA?

**Q12:** Is a tRNA a protein or something else?

**Q13:** How many tRNAs do you expect to be in your genome?

**Q14:** Do you expect to find a tRNA for each codon?

**In galaxy use the `tRNA and tmRNA prediction (Aragorn)` tool to predict tRNA and tmRNAs. Choose the right sequence file and click execute.**

**Q15:** How many tRNAs do you find? (Perhaps compare your result with other groups who have a different organism)

**Q16:** Did you expect to find more or less tRNAs?

**Q17:** Are all codons you expected to be covered by a tRNA found?

## Predicting rRNAs

**Q18:** What is an rRNA?

**Q19:** Which rRNAs do you expect to find for your organism?

**rRNA prediction:**

To predict rRNAs we will use barrnap. In galaxy select the program from the Tools section (left). Make sure you select the genome fasta file and if you want to process the sequences further make sure you either include them in GFF or FASTA file. Click on execute.

**Q20:** Which and how many rRNAs did you find?

## Predicting CRISPR

**Q21:** What is CRISPR in general?

**Q22:** What are the CRISPR repeats that are in the genome?

**Q23:** What is the function of these repeats and the sequence in between?

**To identify if your strain has CRISPR repeats (not all do) you can start the CRISPR Recognition Tool (CRT) from Galaxy. Select your scaffolds.fasta as input and click execute.**

Use the following example if your genome does not have a CRISPR system (or is not found).

| 1                                    | 2       |
| ------------------------------------ | ------- |
| NODE_1_length_1246052_cov_357.753205 | 2831709 |
| NODE_1_length_1246052_cov_357.753205 | 2831751 |
| NODE_1_length_1246052_cov_357.753205 | 3298031 |
| NODE_1_length_1246052_cov_357.753205 | 3298079 |
| NODE_1_length_1246052_cov_357.753205 | 3298139 |

| 3                    | 4                                        |
| -------------------- | ---------------------------------------- |
| CCAGTGACTCCGAGTCACC  | GGGTAAGGAGGCGATCAACCCAA                  |
| CCAGTGACTCCGAGTCACC  | GGCCAGTGACAAAACGATCAGTGAC                |
| AAGCCTGCGGACGAGAAGCC | GGCGGACGAGAATCCCGTCACGGACGAG             |
| AAGCCTGCGGACGAGAAGCC | GGCCGATGAGAAGCCGGCCGACGACGAGCCCGTCGAGGAC |
| AAGCCTGCGGACGAGAAGCC | TGCGGACGAGAAGCCTGCCGACGAGAAGCCGGCCGACGAG |

| 5    | 6    |
| ---- | ---- |
| 19   | 23   |
| 19   | 25   |
| 20   | 28   |
| 20   | 40   |
| 20   | 40   |

**Q24:** How many different CRISPR repeats are found?

**Q25:** How many different CRISPR target sequences are found?


*Later we will learn how to compare sequences against a database. Perhaps you can then identify what the sequences are targeting?*

**Do not forget to update your labjournal with all the findings you have!**

# Bonus

You have used different tools today using the genome as input. It is possible to run this process using a workflow in Galaxy.

At the top of the screen there is a button for `workflow`, here you can create new workflows.

You start with an input dataset and the input which is currently not defined we would like to link to prodigal (see image).

Can you chain the different tools used today and or yesterday to reduce some work?

![](../../Figures/GalaxyWorkflows.png)

Make sure you save the workflow and hit `run` at the top right corner of your screen.