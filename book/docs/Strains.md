# Strain overview

| #  | ID    | Location                    | Source        |
|----|-------|-----------------------------|---------------|
| 9  | C30   | 52°06'40.4"N 5°06'27.6"E    | Technological |
| 6  | M75   | 21°21'02.8"N 158°00'46.7"W  | Technological |
| 10 | 1-13  | 5°43'59.945" / 52°7'31.346" | Both          |
| 5  | M35   | 51°29'06.6"N / 6°10'18.9"E  | Technological |
| 7  | M50   | 52°06'40.4"N 5°06'27.6"E    | Technological |
| 1  | C12   | ?                           | Environment   |
| 2  | 2-32  | 5°43'59.945" / 52°7'31.346" | Environment   |
| 3  | 2-30  | 5°43'59.945" / 52°7'31.346" | Environment   |
| 4  | 1.4.2 | 5°59'59.669" / 52°0'55.141" | Environment   |
| 8  | 2-36  | 5°43'59.945" / 52°7'31.346" | Environment   |

<!-- 
|  Lab code |      Read1     |      Read2     |            Fasta            |               rRNA               |               Scaffolds               |                             Info                             |    Latitude   |   Longitude  |
|:---------:|:--------------:|:--------------:|:---------------------------:|:--------------------------------:|:-------------------------------------:|:------------------------------------------------------------:|:-------------:|:------------:|
| C4.1      | C4_1_1.fq      | C4_1_2.fq      | C4_1_scaffolds.filter.fasta | C4_1_2_scaffolds.fasta.gff       | C4_1_2_scaffolds.fasta.out.fasta      |                                                              | 52°7'31.346"  | 5°43'59.945" |
| C4.2      | C4_2_1.fq      | C4_2_2.fq      | C4_2_scaffolds.filter.fasta | C4_2_2_scaffolds.fasta.gff       | C4_2_2_scaffolds.fasta.out.fasta      |                                                              | 52°7'31.346"  | 5°43'59.945" |
| C7.1      | C7_1_1.fq      | C7_1_2.fq      | C7_1_scaffolds.filter.fasta | C7_1_2_scaffolds.fasta.gff       | C7_1_2_scaffolds.fasta.out.fasta      |                                                              | 52°7'31.346"  | 5°43'59.945" |
| C7.2      | C7_2_1.fq      | C7_2_2.fq      | C7_2_scaffolds.filter.fasta | C7_2_2_scaffolds.fasta.gff       | C7_2_2_scaffolds.fasta.out.fasta      |                                                              | 52°7'31.346"  | 5°43'59.945" |
| C8        | C8_1.fq        | C8_2.fq        | C8_scaffolds.filter.fasta   | C8_2_scaffolds.fasta.gff         | C8_2_scaffolds.fasta.out.fasta        |                                                              | 52°7'31.346"  | 5°43'59.945" |
| C11       | C11_1.fq       | C11_2.fq       | C11_scaffolds.filter.fasta  | C11_2_scaffolds.fasta.gff        | C11_2_scaffolds.fasta.out.fasta       | Isolated in the lab around 1956, Anthonie van L. nr38 (1972) |               |              |
| C12       | C12_1.fq       | C12_2.fq       | C12_scaffolds.filter.fasta  | C12_2_scaffolds.fasta.gff        | C12_2_scaffolds.fasta.out.fasta       | Isolated in 1947 from unknown source                         |               |              |
| C13       | C13_1.fq       | C13_2.fq       | C13_scaffolds.filter.fasta  | C13_2_scaffolds.fasta.gff        | C13_2_scaffolds.fasta.out.fasta       | Unknown                                                      |               |              |
| C14       | C14_1.fq       | C14_2.fq       | C14_scaffolds.filter.fasta  | C14_2_scaffolds.fasta.gff        | C14_2_scaffolds.fasta.out.fasta       | Obtained from Delft L.M.D., used by J. de Bont (1980)        |               |              |
| C15       | C15_1.fq       | C15_2.fq       | C15_scaffolds.filter.fasta  | C15_2_scaffolds.fasta.gff        | C15_2_scaffolds.fasta.out.fasta       | isolated by J.W.Woldendorp, Delft 1961                       |               |              |
| 2-30      | D30_1.fq       | D30_2.fq       | D30_scaffolds.filter.fasta  | D30_2_scaffolds.fasta.gff        | D30_2_scaffolds.fasta.out.fasta       |                                                              | 52°7'31.346"  | 5°43'59.945" |
| 2-32      | D32_1.fq       | D32_2.fq       | D32_scaffolds.filter.fasta  | D32_2_scaffolds.fasta.gff        | D32_2_scaffolds.fasta.out.fasta       |                                                              | 52°7'31.346"  | 5°43'59.945" |
| 2-44      | D44_1.fq       | D44_2.fq       | D44_scaffolds.filter.fasta  | D44_2_scaffolds.fasta.gff        | D44_2_scaffolds.fasta.out.fasta       |                                                              | 52°7'31.346"  | 5°43'59.945" |
| 2-37      | D37_1.fq       | D37_2.fq       | D37_scaffolds.filter.fasta  | D37_2_scaffolds.fasta.gff        | D37_2_scaffolds.fasta.out.fasta       |                                                              | 52°7'31.346"  | 5°43'59.945" |
| 2-3       | D3_1.fq        | D3_2.fq        | D3_scaffolds.filter.fasta   | D3_2_scaffolds.fasta.gff         | D3_2_scaffolds.fasta.out.fasta        |                                                              | 52°7'31.346"  | 5°43'59.945" |
| 2-34      | D34_1.fq       | D34_2.fq       | D34_scaffolds.filter.fasta  | D34_2_scaffolds.fasta.gff        | D34_2_scaffolds.fasta.out.fasta       |                                                              | 52°7'31.346"  | 5°43'59.945" |
| 2-36      | D36_1.fq       | D36_2.fq       | D36_scaffolds.filter.fasta  | D36_2_scaffolds.fasta.gff        | D36_2_scaffolds.fasta.out.fasta       |                                                              | 52°7'31.346"  | 5°43'59.945" |
| 2-22      | D22_1.fq       | D22_2.fq       | D22_scaffolds.filter.fasta  | D22_2_scaffolds.fasta.gff        | D22_2_scaffolds.fasta.out.fasta       |                                                              | 52°7'31.346"  | 5°43'59.945" |
| 26I1      | S26I1_1.fq     | S26I1_2.fq     | S26I1_2_scaffolds.fasta     | S26I1_2_scaffolds.fasta.gff      | S26I1_2_scaffolds.fasta.out.fasta     |                                                              | 52°12'37.418" | 4°24'0.86"   |
| 1.3.1     | S1_3_1_1.fq    | S1_3_1_2.fq    | S1_3_1_2_scaffolds.fasta    | S1_3_1_2_scaffolds.fasta.txt.gff | S1_3_1_2_scaffolds.fasta.txt.fasta    |                                                              | 52°0'55.141"  | 5°59'59.669" |
| 1.4.2     | S1_4_2_1.fq    | S1_4_2_2.fq    | S1_4_2_2_scaffolds.fasta    | S1_4_2_2_scaffolds.fasta.gff     | S1_4_2_2_scaffolds.fasta.out.fasta    |                                                              | 52°0'55.141"  | 5°59'59.669" |
| 1.1.1.XLD | S1_1_1xld_1.fq | S1_1_1xld_2.fq | S1_1_1xld_2_scaffolds.fasta | S1_1_1xld_2_scaffolds.fasta.gff  | S1_1_1xld_2_scaffolds.fasta.out.fasta |                                                              | 52°0'55.141"  | 5°59'59.669" |
| 1.4.1 XLD | S1_1_4XLD_1.fq | S1_1_4XLD_2.fq | S1_1_4XLD_2_scaffolds.fasta | S1_1_4XLD_2_scaffolds.fasta.gff  | S1_1_4XLD_2_scaffolds.fasta.out.fasta |                                                              | 52°0'55.141"  | 5°59'59.669" | -->