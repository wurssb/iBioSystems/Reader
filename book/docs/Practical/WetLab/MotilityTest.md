# Motility Test

## Introduction
Cell motility allows bacteria to move through their environment in search of optimal growth conditions. This movement is driven by flagella, long appendages that protrude from the bacterial cell body. While flagella are commonly associated with motility, they may also serve as sensory organelles for e.g., light, chemical concentrations, or temperatures. Some motile bacteria use such sensing information to navigate to more suited environments. Microbes have been described that can navigate based on chemical concentrations (chemotaxis), light (phototaxis), temperatures (thermotaxis), and even magnetic fields (magnetotaxis).

## Protocol

### Day 1: Set up experiment
* Take a glass tube with semisolid LB agar medium.
    * **Important**: Do not shake or stir the tube!
* Inoculate with a straight wire, by first dipping the wire in the liquid overnight culture and then making a single stab down the center of the tube to about 
half the depth of the medium. 
* Incubate at 30°C or 37°C, depending on the optimal growth temperature of your strain.

### Day 2: Observe results
* Examine growth after 1 and 2 days (depends on the generation time of bacteria):
    * Non-motile bacteria generally show growth patterns that are confined to the stab line, have sharply defined margins, and leave the surrounding medium clearly transparent. 
    * Motile bacteria typically show diffuse, hazy growth patterns that spread throughout the medium rendering it slightly opaque (Figure 1).

![](MotilityTest.png)

*Figure 1: Result of motility test. Left: motile bacteria; Middle: motile bacteria; Right: non-motile bacteria*

## References

Madigan, M., Bender, K., Buckley, D., Martinko, J., Brock, T. & Stahl, D. (2014). Brock biology of microorganisms (14th ed., pp.80-87).

Patricia Shields , Laura Cathcart, (2011) MOTILITY TEST MEDIUM PROTOCOL,  American Society for Microbiology, November 2011, protocols 
