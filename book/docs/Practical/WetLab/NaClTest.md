# NaCl test

## Introduction 

Halotolerant bacteria have the ability to grow in the presence of high concentrations of salt (NaCl). They use different strategies to maintain their cytoplasm under constant osmotic conditions in relation with the extracellular environment. Moreover, they have developed different systems to export the salt out of the cell. The highly salt-tolerant enzymes that halotolerant bacteria possess are potentially useful for e.g. treatment of saline waste water. 

The distribution of halobacteria is one of the widest among the extremophiles (i.e., those organisms that live in extreme conditions, such as T, pH, or salinity). Several lifeforms are able to grow in the presence of high concentrations of sodium chloride (NaCl) or common salt. Note the distinction between halotolerant and halophilic organisms (Figure): The halotolerant microorganisms can survive in saline environments but they grow optimally in non-saline environments, while halophilic microorganisms require the presence of high concentrations of salt to grow optimally or even to survive. 

![](NaCl.png)

*Figure: NaCl-related classification of bacteria*

<span style="color:white">\newpage</span>

## Protocol

### Day 1: Set up experiment

* Collect your plates with varying NaCl concentrations (1%, 2.5%, 5%, 7.5%, 10%, 12.5% and 20%)
* Make a suspension of a single colony from your mother plate in 0.5 ml of sterile water.
* Inoculate each plate with an inoculation loop using the Zig-Zag method.
* Incubate the plates for 24-48 hours at the preferred temperature.

### Day 2/3: Observe results

* Check for growth (presence of colonies). If possible, count the colonies. 

## References

Oren, A. (2002) Diversity of halophilic microorganisms: Environments, phylogeny, physiology and applications. *Journal of Industrial Microbiology and Biotechnology.* 28, pp.56-63.

Oren, A. (2008). Microbial life at high salt concentrations: phylogenetic and metabolic diversity. *Saline Systems*, *4*(1), 2.

Kushner, D.J. (1978) Life in high salt and solute concentrations: halophilic bacteria, in Microbial Life in Extreme Environments (Kushner, D.J., Ed.), pp. 317-368. Academic Press, London.

Larsen, H. (1986) Halophilic and halotolerant microorganisms – an overview and historical perspective. *FEMS Microbiology Reviews.* 39, pp.3-7.
